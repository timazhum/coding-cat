﻿/* 
 * Name: csViewMiniGameOperators.cs
 * Description: a view of an operators mini game
 * Author: Temirlan Zhumanov
 * Date: 24.04.2016
 * Email: lebrokholic@gmail.com
 * Copyright: All Rights Reserved.
 */

using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;
using System.Collections;
using System.Collections.Generic;

public class csViewMiniGameOperators: csView
{
	/// <summary>
	/// The texts of the numbers
	/// </summary>
	public List<Text> _textsNumbers = new List<Text>();

	/// <summary>
	/// The buttons of digits
	/// </summary>
	public List<Button> _buttonsDigits = new List<Button>();

	/// <summary>
	/// Updates this view
	/// </summary>
	/// <param name="_arguments">Array of arguments</param>
	public override void UpdateView(object[] _arguments)
	{
		csArgumentsReader _argumentsReader = new csArgumentsReader(_arguments);
		if (_argumentsReader != null)
		{
			object _numbersObject = _argumentsReader.GetValue(csMiniGameOperators.NUMBERS);
			if (_numbersObject is List<int>)
			{
				List<int> _numbers = _numbersObject as List<int>;
				if (_textsNumbers != null && _numbers != null && _textsNumbers.Count == _numbers.Count)
					for (int i = 0; i < _textsNumbers.Count; i++)
						if (_textsNumbers[i])
							_textsNumbers[i].text = System.Convert.ToString(_numbers[i], 2).PadLeft(4, '0');
			}

			object _guessNumberObject = _argumentsReader.GetValue(csMiniGameOperators.GUESSNUMBER);
			if (_guessNumberObject is int)
			{
				int _guessNumber = (int)_guessNumberObject;
				if (_buttonsDigits != null)
					for (int i = 0; i < _buttonsDigits.Count; i++)
					{
						Text _text = _buttonsDigits[i] && _buttonsDigits[i].transform ?
							_buttonsDigits[i].transform.GetComponentInChildren<Text>() : null;
						if (_text)
							_text.text = (_guessNumber & (1 << (_buttonsDigits.Count - i - 1))) != 0 ? "1" : "0";				
					}
			}				
		}
	}

	/// <summary>
	/// Inverts the digit
	/// </summary>
	/// <param name="_position">Position of a digit</param>
	public void InvertDigit(int _position)
	{
		if (_controller && _controller is csMiniGameOperators)
			(_controller as csMiniGameOperators).InvertDigit(_position);
	}
}

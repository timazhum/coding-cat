﻿/* 
 * Name: csViewMiniGameNumber.cs
 * Description: a view of a numbers mini game
 * Author: Temirlan Zhumanov
 * Date: 24.04.2016
 * Email: lebrokholic@gmail.com
 * Copyright: All Rights Reserved.
 */

using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;
using System.Collections;
using System.Collections.Generic;

public class csViewMiniGameNumbers: csView
{
	/// <summary>
	/// The text of the binary number
	/// </summary>
	public Text _textNumber = null;

	/// <summary>
	/// The text of the stats
	/// </summary>
	public Text _textStats = null;

	/// <summary>
	/// The buttons of variants
	/// </summary>
	public List<Button> _buttonsVariants = new List<Button>();

	/// <summary>
	/// Updates this view
	/// </summary>
	/// <param name="_arguments">Array of arguments</param>
	public override void UpdateView(object[] _arguments)
	{
		csArgumentsReader _argumentsReader = new csArgumentsReader(_arguments);
		if (_argumentsReader != null)
		{
			object _guessNumber = _argumentsReader.GetValue(csMiniGameNumbers.GUESSNUMBER);
			if (_guessNumber is string && _textNumber)
				_textNumber.text = _guessNumber as string;

			object _successNumber = _argumentsReader.GetValue(csMiniGameNumbers.SUCCESSNUMBER),
				_iterationsCurrent = _argumentsReader.GetValue(csMiniGameNumbers.ITERATIONSCURRENT),
				_iterationsMax = _argumentsReader.GetValue(csMiniGameNumbers.ITERATIONSMAX);
			if (_successNumber is int && _iterationsCurrent is int && _iterationsMax is int &&  _textStats)
				_textStats.text = ((int)_successNumber).ToString() + " correct\n" +
					((int)_iterationsCurrent).ToString() + "/" + ((int)_iterationsMax).ToString();

			object _variantsObject = _argumentsReader.GetValue(csMiniGameNumbers.VARIANTS);
			if (_variantsObject is List<int>)
			{
				List<int> _variants = _variantsObject as List<int>;
				if (_buttonsVariants != null && _variants != null && _buttonsVariants.Count == _variants.Count)
					for (int i = 0; i < _buttonsVariants.Count; i++)
					{
						Text _text = _buttonsVariants[i] && _buttonsVariants[i].transform ?
							_buttonsVariants[i].transform.GetComponentInChildren<Text>() : null;
						if (_text) _text.text = _variants[i].ToString();
					}
			}
		}
	}

	/// <summary>
	/// Makes the guess
	/// </summary>
	/// <param name="_variantIndex">Variant index</param>
	public void MakeGuess(int _variantIndex)
	{
		if (_controller && _controller is csMiniGameNumbers)
			(_controller as csMiniGameNumbers).MakeGuess(_variantIndex);
	}
}

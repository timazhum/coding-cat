﻿/* 
 * Name: csViewActivityItemsItem.cs
 * Description: a view of an item in the items activity
 * Author: Temirlan Zhumanov
 * Date: 24.04.2016
 * Email: lebrokholic@gmail.com
 * Copyright: All Rights Reserved.
 */

using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;
using System.Collections;
using System.Collections.Generic;

public class csViewActivityItemsItem: csView
{	
	/// <summary>
	/// The image of an item
	/// </summary>
	public Image _image = null;

	/// <summary>
	/// The name of an item
	/// </summary>
	public Text _textName = null;

	/// <summary>
	/// The status of an item
	/// </summary>
	public Text _textStatus = null;

	/// <summary>
	/// Updates this view
	/// </summary>
	/// <param name="_arguments">Array of arguments</param>
	public override void UpdateView(object[] _arguments)
	{
		csArgumentsReader _argumentsReader = new csArgumentsReader(_arguments);
		if (_argumentsReader != null)
		{
			Sprite _sprite = _argumentsReader.GetValue(csItem.IMAGE) as Sprite;
			if (_sprite != null && _image) _image.sprite = _sprite;

			if (_argumentsReader.GetValue(csItem.NAME) is string)
				if (_textName) _textName.text = (string)_argumentsReader.GetValue(csItem.NAME);				

			if (_argumentsReader.GetValue(csItem.PURCHASED) is bool)
			{
				bool _purchased = (bool)_argumentsReader.GetValue(csItem.PURCHASED);
				if (_purchased)
				{
					if (_argumentsReader.GetValue(csItem.ACTIVATED) is bool)
						if (_textStatus) _textStatus.text = (bool)_argumentsReader.GetValue(csItem.ACTIVATED) ? "Active" : "Inactive";
				}
				else
				{
					object _cost = _argumentsReader.GetValue(csItem.COST);
					if (_cost is int && _textStatus) _textStatus.text = "$" + ((int)_cost).ToString();
				}
			}
		}
	}

	/// <summary>
	/// When the item is clicked
	/// </summary>
	public void OnItemClick()
	{
		if (_controller && _controller is csItem)
		{
			csItem _item = _controller as csItem;
			if (_item)
			{
				if (_item._purchased)
					_item._activated = !_item._activated;
				else
					_item.Purchase();
			}
		}
	}
}

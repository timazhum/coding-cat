﻿/* 
 * Name: csViewItem.cs
 * Description: a view of an item
 * Author: Temirlan Zhumanov
 * Date: 24.04.2016
 * Email: lebrokholic@gmail.com
 * Copyright: All Rights Reserved.
 */

using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;
using System.Collections;
using System.Collections.Generic;

public class csViewItem: csView
{		
	/// <summary>
	/// Updates this view
	/// </summary>
	/// <param name="_arguments">Array of arguments</param>
	public override void UpdateView(object[] _arguments)
	{
		csArgumentsReader _argumentsReader = new csArgumentsReader(_arguments);
		if (_argumentsReader != null)
		{
			if (_argumentsReader.GetValue(csItem.ACTIVATED) is bool)
				if (gameObject)
					gameObject.SetActive((bool)_argumentsReader.GetValue(csItem.ACTIVATED));
		}
	}		
}

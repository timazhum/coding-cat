﻿/* 
 * Name: csController.cs
 * Description: an abstract class of a controller in the MVC pattern
 * Author: Temirlan Zhumanov
 * Date: 22.04.2016
 * Email: lebrokholic@gmail.com
 * Copyright: All Rights Reserved.
 */


using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public abstract class csController: csClass
{
	/// <summary>
	/// The model of a contoller
	/// </summary>
	public csModel _model;

	/// <summary>
	/// The views of a controller
	/// </summary>
	public List<csView> _views;

	/// <summary>
	/// Updates the model
	/// </summary>
	/// <param name="_arguments">Array of arguments</param>
	public void UpdateModel(object[] _arguments)
	{
		if (_model) _model.UpdateModel(_arguments);
	}

	/// <summary>
	/// Updates the view
	/// </summary>
	public abstract void UpdateView(csView _view);

	/// <summary>
	/// Updates the views
	/// </summary>
	public void UpdateViews()
	{
		foreach (csView _view in _views)
			UpdateView(_view);
	}
}

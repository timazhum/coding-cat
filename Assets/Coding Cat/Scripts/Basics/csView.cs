﻿/* 
 * Name: csView.cs
 * Description: an abstract class of a view in the MVC pattern
 * Author: Temirlan Zhumanov
 * Date: 22.04.2016
 * Email: lebrokholic@gmail.com
 * Copyright: All Rights Reserved.
 */

using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public abstract class csView: csListener
{
	/// <summary>
	/// The model of a view
	/// </summary>
	public csModel _model;
	
	/// <summary>
	/// The controller of a view
	/// </summary>
	public csController _controller;

	/// <summary>
	/// Updates this listener
	/// </summary>
	/// <param name="_arguments">Array of arguments</param>
	public override void UpdateListener(object[] _arguments)
	{
		UpdateView(_arguments);
	}

	/// <summary>
	/// Updates this view
	/// </summary>
	/// <param name="_arguments">Array of arguments</param>
	public abstract void UpdateView(object[] _arguments);

	/// <summary>
	/// Awake this instance
	/// </summary>
	protected override void Awake()
	{		
		base.Awake();

		if (_model) _model.AddListener(this);
	}
}

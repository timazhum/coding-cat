﻿/* 
 * Name: csClass.cs
 * Description: a basic abstract class
 * Author: Temirlan Zhumanov
 * Date: 22.04.2016
 * Email: lebrokholic@gmail.com
 * Copyright: All Rights Reserved.
 */

using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public abstract class csClass: MonoBehaviour
{
	/// <summary>
	/// The sorted dictionary of strings and lists of classes instances
	/// </summary>
	protected static SortedDictionary<string, List<csClass>> _classesInstances = new SortedDictionary<string, List<csClass>>();

	/// <summary>
	/// Awake this instance
	/// </summary>
	protected virtual void Awake()
	{
		AddInstance(this, GetType());
	}

	/// <summary>
	/// On this instance destroyed
	/// </summary>
	protected virtual void OnDestroy()
	{
		RemoveInstance(this, GetType());
	}

	/// <summary>
	/// Returns the list of class instances
	/// </summary>
	/// <returns>The class instances</returns>
	/// <param name="_name">Name of a class</param>
	protected static List<csClass> GetClassInstances(string _name)
	{		
		if (!_classesInstances.ContainsKey(_name))
			_classesInstances.Add(_name, new List<csClass>());

		List<csClass> _list = null;
		_classesInstances.TryGetValue(_name, out _list);
		return _list;
	}

	/// <summary>
	/// Adds the instance of a class
	/// </summary>
	/// <param name="_instance">Instance of a class</param>
	/// <param name="_type">Type of the specific instance</param>
	protected static void AddInstance(csClass _instance, System.Type _type)
	{
		List<csClass> _classInstances = GetClassInstances(_type.Name);
		if (_classInstances != null) _classInstances.Add(_instance);
	}

	/// <summary>
	/// Removes the instance of a class
	/// </summary>
	/// <param name="_instance">Instance of a class</param>
	/// <param name="_type">Type of the specific instance</param>
	protected static void RemoveInstance(csClass _instance, System.Type _type)
	{
		List<csClass> _classInstances = GetClassInstances(_type.Name);
		if (_classInstances != null) _classInstances.Remove(_instance);
	}

	/// <summary>
	/// Returns the first instance of a class
	/// </summary>
	/// <returns>The first instance of a class</returns>
	/// <param name="_type">Type of the specific instance</param>
	public static csClass GetInstance(System.Type _type)
	{
		List<csClass> _classInstances = GetClassInstances(_type.Name);
		return _classInstances != null && _classInstances.Count > 0 ? _classInstances[0] : null;
	}

	/// <summary>
	/// Returns the specific instance of a class
	/// </summary>
	/// <returns>The specific instance of a class</returns>
	/// <param name="_index">Index of the specific instance</param>
	/// <param name="_type">Type of the specific instance</param>
	public static csClass GetInstance(int _index, System.Type _type)
	{
		List<csClass> _classInstances = GetClassInstances(_type.Name);
		return _classInstances != null && _index >= 0 && _index < _classInstances.Count ? _classInstances[_index] : null;
	}

	/// <summary>
	/// Returns the instances list
	/// </summary>
	/// <returns>The instances list</returns>
	/// <param name="_type">Type of the specific instance</param>
	public static List<csClass> GetInstances(System.Type _type)
	{
		return GetClassInstances(_type.Name);
	}

	/// <summary>
	/// Returns the first instance of this class
	/// </summary>
	/// <returns>The first instance of this class</returns>
	public csClass GetInstance()
	{
		return GetInstance(GetType());
	}

	/// <summary>
	/// Returns the specific instance of this class
	/// </summary>
	/// <returns>The specific instance of this class</returns>
	/// <param name="_index">Index of the specific instance of this class</param>
	public csClass GetInstance(int _index)
	{
		return GetInstance(_index, GetType());
	}

	/// <summary>
	/// Returns the instances list of this class
	/// </summary>
	/// <returns>The instances list of this class</returns>
	public List<csClass> GetInstances()
	{
		return GetInstances(GetType());
	}
}

﻿/* 
 * Name: csModel.cs
 * Description: an abstract class of a model in the MVC pattern
 * Author: Temirlan Zhumanov
 * Date: 22.04.2016
 * Email: lebrokholic@gmail.com
 * Copyright: All Rights Reserved.
 */

using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public abstract class csModel: csSensor
{
	/// <summary>
	/// Updates this model
	/// </summary>
	/// <param name="_arguments">Array of arguments</param>
	public abstract void UpdateModel(object[] _arguments);

	/// <summary>
	/// Updates the views of the model
	/// </summary>
	/// <param name="_arguments">Array of arguments</param>
	public void UpdateViews(object[] _arguments)
	{
		NotifyListeners(_arguments);
	}
}

﻿/* 
 * Name: csListener.cs
 * Description: an abstract class of a listener in the observer pattern
 * Author: Temirlan Zhumanov
 * Date: 22.04.2016
 * Email: lebrokholic@gmail.com
 * Copyright: All Rights Reserved.
 */

using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public abstract class csListener: csClass
{
	/// <summary>
	/// Updates this listener
	/// </summary>
	/// <param name="_arguments">Arrya of arguments</param>
	public abstract void UpdateListener(object[] _arguments);
}

﻿/* 
 * Name: csSingletonComponent.cs
 * Description: a singleton component
 * Author: Temirlan Zhumanov
 * Date: 23.04.2016
 * Email: lebrokholic@gmail.com
 * Copyright: All Rights Reserved.
 */

using UnityEngine;
using System.Collections;

public abstract class csSingletonComponent: csComponent
{
	/// <summary>
	/// Awake this instance
	/// </summary>
	protected override void Awake()
	{
		if (GetInstance()) // if an instance of a component exists
		{			
			Destroy(gameObject); // destroy the object
		}
		else
		{
			base.Awake(); // call the super class method
			DontDestroyOnLoad(gameObject); // do not destroy this object
		}
	}
}

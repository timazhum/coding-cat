﻿/* 
 * Name: csArgumentsReader.cs
 * Description: a class of an arguments reader
 * Author: Temirlan Zhumanov
 * Date: 23.04.2016
 * Email: lebrokholic@gmail.com
 * Copyright: All Rights Reserved.
 */

using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public sealed class csArgumentsReader
{
	/// <summary>
	/// The arguments dictionary
	/// </summary>
	private SortedDictionary<string, object> _arguments = new SortedDictionary<string, object>();

	/// <summary>
	/// Initializes a new instance of the <see cref="csArgumentsReader"/> class
	/// </summary>
	public csArgumentsReader(object[] _arguments)
	{
		if (_arguments != null && _arguments.Length > 0 &&
			_arguments[0] != null && _arguments[0] is SortedDictionary<string, object>)
		{
			this._arguments = _arguments[0] as SortedDictionary<string, object>;
		}
		else
		{
			throw (new UnityException("Cannot read the arguments data"));
		}
	}

	/// <summary>
	/// Determines whether the arguments contain the specific key or not
	/// </summary>
	/// <returns><c>true</c> if the arguments contain the specific key; otherwise, <c>false</c>.</returns>
	/// <param name="_key">Key</param>
	public bool HasKey(string _key)
	{
		return _arguments.ContainsKey(_key);
	}

	/// <summary>
	/// Returns the value by a key
	/// </summary>
	/// <returns>The value</returns>
	/// <param name="_key">Key</param>
	public object GetValue(string _key)
	{
		object _value = null;
		_arguments.TryGetValue(_key, out _value);
		return _value;
	}
}

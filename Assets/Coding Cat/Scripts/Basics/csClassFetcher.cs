﻿/* 
 * Name: csClassFetcher.cs
 * Description: a static class for fetching classes
 * Author: Temirlan Zhumanov
 * Date: 22.04.2016
 * Email: lebrokholic@gmail.com
 * Copyright: All Rights Reserved.
 */

using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public static class csClassFetcher<T>
{
	/// <summary>
	/// Returns the first instance of a class
	/// </summary>
	/// <returns>The first instance of a class</returns>
	public static T GetInstance()
	{
		return (T)(object)csClass.GetInstance(typeof (T));
	}

	/// <summary>
	/// Returns the specific instance of a class
	/// </summary>
	/// <returns>The specific instance of a class</returns>
	/// <param name="_index">Index of the specific instance</param>
	public static T GetInstance(int _index)
	{
		return (T)(object)csClass.GetInstance(_index, typeof (T));
	}

	/// <summary>
	/// Returns the instances list
	/// </summary>
	/// <returns>The instances list</returns>
	public static List<T> GetInstances()
	{
		List<csClass> _rawList = csClass.GetInstances(typeof (T));
		List<T> _fetchedList = new List<T>();

		foreach (csClass _class in _rawList)
			if (_class != null)
				_fetchedList.Add((T)(object)_class);

		return _fetchedList;
	}
}

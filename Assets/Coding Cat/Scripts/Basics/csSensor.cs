﻿/* 
 * Name: csSensor.cs
 * Description: an abstract class of a sensor in the observer pattern
 * Author: Temirlan Zhumanov
 * Date: 22.04.2016
 * Email: lebrokholic@gmail.com
 * Copyright: All Rights Reserved.
 */

using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public abstract class csSensor: csClass
{
	/// <summary>
	/// The has set of this sensor's listeners
	/// </summary>
	protected HashSet<csListener> _listeners = new HashSet<csListener>();

	/// <summary>
	/// Adds the listener to this sensor
	/// </summary>
	/// <param name="_listener">The specific listener</param>
	public void AddListener(csListener _listener)
	{
		if (_listeners != null) _listeners.Add(_listener);
	}

	/// <summary>
	/// Removes the listener from this sensor
	/// </summary>
	/// <param name="_listener">The specific listener</param>
	public void RemoveListener(csListener _listener)
	{
		if (_listeners != null) _listeners.Remove(_listener);
	}

	/// <summary>
	/// Notifies all the listeners of this sensor
	/// </summary>
	/// <param name="_arguments">Array of arguments</param>
	public void NotifyListeners(object[] _arguments)
	{
		if (_listeners != null) 
			foreach (csListener _listener in _listeners)
				_listener.UpdateListener(_arguments);
	}
}

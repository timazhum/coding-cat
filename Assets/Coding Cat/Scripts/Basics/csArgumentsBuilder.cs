﻿/* 
 * Name: csArgumentsBuilder.cs
 * Description: a class of an arguments builder
 * Author: Temirlan Zhumanov
 * Date: 23.04.2016
 * Email: lebrokholic@gmail.com
 * Copyright: All Rights Reserved.
 */

using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public sealed class csArgumentsBuilder
{
	/// <summary>
	/// The arguments dictionary
	/// </summary>
	private SortedDictionary<string, object> _arguments = new SortedDictionary<string, object>();

	/// <summary>
	/// Initializes a new instance of the <see cref="csArgumentsBuilder"/> class
	/// </summary>
	public csArgumentsBuilder() {}

	/// <summary>
	/// Adds the specified _key and _value to the arguments
	/// </summary>
	/// <param name="_key">Key</param>
	/// <param name="_value">Value</param>
	public csArgumentsBuilder Add(string _key, object _value)
	{
		_arguments.Add(_key, _value);
		return this;
	}

	/// <summary>
	/// Removes the specified _key from the arguments
	/// </summary>
	/// <param name="_key">Key</param>
	public csArgumentsBuilder Remove(string _key)
	{
		_arguments.Remove(_key);
		return this;
	}

	/// <summary>
	/// Builds the arguments
	/// </summary>
	public object[] Build()
	{
		return new object[] { _arguments };
	}
}

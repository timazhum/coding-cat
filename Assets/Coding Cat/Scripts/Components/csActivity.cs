﻿/* 
 * Name: csActivity.cs
 * Description: an abstract component of an activity
 * Author: Temirlan Zhumanov
 * Date: 23.04.2016
 * Email: lebrokholic@gmail.com
 * Copyright: All Rights Reserved.
 */

using UnityEngine;
using UnityEngine.UI;
using UnityEngine.UI.Extensions;
using System.Collections;
using System.Collections.Generic;

public abstract class csActivity: csComponent
{
	/// <summary>
	/// Sets the activity's instance active.
	/// </summary>
	/// <param name="_value">If set to <c>true</c>, then activates the instance, otherwise, deactivates it.</param>
	public void SetActive(bool _value)
	{
		if (gameObject) gameObject.SetActive(_value); // set the game object active
	}
	
	/// <summary>
	/// Gets the name of the activity's instance
	/// </summary>
	/// <returns>The name of the activity's instance</returns>
	public abstract string GetName();

	/// <summary>
	/// Updates the UI
	/// </summary>
	public abstract void UpdateUI(object[] _arguments);
}

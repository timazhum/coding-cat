﻿/* 
 * Name: csHotkey.cs
 * Description: a component of a hotkey
 * Author: Temirlan Zhumanov
 * Date: 23.04.2016
 * Email: lebrokholic@gmail.com
 * Copyright: All Rights Reserved.
 */

using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;
using System.Collections;

public class csHotkey: csComponent
{
	/// <summary>
	/// The name of a hotkey
	/// </summary>
	public string _hotkey = "Cancel";

	/// <summary>
	/// The button which controls a hotkey
	/// </summary>
	public Button _button = null;

	/// <summary>
	/// Update this instance
	/// </summary>
	protected void Update()
	{
		if (Input.GetButtonDown(_hotkey) && _button) // if hotkey is pressed
			_button.onClick.Invoke(); // invoke the method on the button
	}
}

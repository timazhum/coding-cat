﻿/* 
 * Name: csApplication.cs
 * Description: a component of an application
 * Author: Temirlan Zhumanov
 * Date: 23.04.2016
 * Email: lebrokholic@gmail.com
 * Copyright: All Rights Reserved.
 */

using UnityEngine;
using System.Collections;
using UnityEngine.SceneManagement;

public class csApplication: csComponent
{	
	/// <summary>
	/// The cached value of a time scale
	/// </summary>
	private float _cachedTimeScale = 1f;

	/// <summary>
	/// Is the game paused or not
	/// </summary>
	private bool _paused = false;

	/// <summary>
	/// The time scale change delta
	/// </summary>
	private float _timeScaleChangeDelta = 0.1f;

	/// <summary>
	/// The cached GUI instance
	/// </summary>
	private csGUI _gui = null;

	/// <summary>
	/// Awake this instance
	/// </summary>
	protected override void Awake ()
	{
		base.Awake();

//		PlayerPrefs.DeleteAll();
	}

	/// <summary>
	/// Start this instance
	/// </summary>
	protected void Start()
	{
		_gui = csClassFetcher<csGUI>.GetInstance(); // cache the GUI instance

		if (_gui) _gui.Curtain(false); // hide the curtain
	}

	/// <summary>
	/// Quit from the application
	/// </summary>
	public void Quit()
	{
		Application.Quit(); // quit the application
		Debug.Log("" + Time.time + ": Quit the application");
	}	

	/// <summary>
	/// Pause the application
	/// </summary>
	public void Pause()
	{
		_cachedTimeScale = !Mathf.Approximately(Time.timeScale, 0f) ? Time.timeScale : _cachedTimeScale; // cache the current time scale
		Time.timeScale = 0f; // set the current time scale
		_paused = true; // check as paused
	}

	/// <summary>
	/// Unpause the application
	/// </summary>
	public void Unpause()
	{
		Time.timeScale = _cachedTimeScale; // set the time scale to the cached one
		_paused = false; // check as unpaused
	}

	/// <summary>
	/// Returns the cached time scale
	/// </summary>
	/// <returns>The cached time scale</returns>
	public float GetCachedTimeScale()
	{
		return _cachedTimeScale;
	}

	/// <summary>
	/// Determines whether this instance is paused
	/// </summary>
	/// <returns><c>true</c> if this instance is paused; otherwise, <c>false</c></returns>
	public bool IsPaused()
	{
		return _paused;
	}

	/// <summary>
	/// Sets the cached time scale
	/// </summary>
	/// <param name="_cachedTimeScale">Cached time scale</param>
	public void SetCachedTimeScale(float _cachedTimeScale)
	{
		this._cachedTimeScale = _cachedTimeScale;
	}		

	/// <summary>
	/// Raised when the quit event happens
	/// </summary>
	protected void OnApplicationQuit()
	{
		
 	}
	
	/// <summary>
	/// Loads the level
	/// </summary>
	/// <param name="_name">The name of the level</param>
	public void LoadLevel(string _name)
	{
		StartCoroutine(LoadLevelHelper(_name)); // execute the helper coroutine
	}

	/// <summary>
	/// Helper for the load level function
	/// </summary>
	/// <param name="_name">The name of the level</param>
	private IEnumerator LoadLevelHelper(string _name)
	{	
		if (_gui) _gui.Curtain(true); // show the curtain

		yield return StartCoroutine(WaitForRealSeconds(csGUI.GetCurtainTransitionTime())); // wait white the curtain is in transition

		SceneManager.LoadScene(_name); // load the specified level
	}

	/// <summary>
	/// Waits for real seconds
	/// </summary>
	/// <param name="_time">Time to wait</param>
	public static IEnumerator WaitForRealSeconds(float _time)
	{
		float _start = Time.realtimeSinceStartup; // the starting time
		while (Time.realtimeSinceStartup < _start + _time) // while the time is not expired
		{
			yield return null; // wait for a frame
		}
	}

	/// <summary>
	/// Sets the game object active
	/// </summary>
	/// <param name="_gameObject">A game object</param>
	public void Activate(GameObject _gameObject)
	{
		if (_gameObject) _gameObject.SetActive(true); // set the object active
	}

	/// <summary>
	/// Sets the game object inactive
	/// </summary>
	/// <param name="_gameObject">A game object</param>
	public void Deactivate(GameObject _gameObject)
	{
		if (_gameObject) _gameObject.SetActive(false); // set the object inactive
	}

	/// <summary>
	/// Switch the specified game object
	/// </summary>
	/// <param name="_gameObject">Game object</param>
	public void Switch(GameObject _gameObject)
	{
		if (_gameObject && _gameObject.activeSelf) Deactivate(_gameObject);
		else Activate(_gameObject);
	}

	/// <summary>
	/// Sets the game object active after an amount of time
	/// </summary>
	/// <param name="_gameObject">A game object</param>
	/// <param name="_active">If set to <c>true</c>, then activates</param>
	/// <param name="_time">A specific amount of time</param>
	public void SetActiveAfter(GameObject _gameObject, bool _active, float _time)
	{
		StartCoroutine(SetActiveAfterHelper(_gameObject, _active, _time)); // start the helper
	}

	/// <summary>
	/// A helper for the set active after function
	/// </summary>
	/// <param name="_gameObject">A game object</param>
	/// <param name="_active">If set to <c>true</c>, then activates</param>
	/// <param name="_time">A specific amount of time</param>
	public IEnumerator SetActiveAfterHelper(GameObject _gameObject, bool _active, float _time)
	{
		yield return new WaitForSeconds(_time); // wait for some time
		if (_gameObject) _gameObject.SetActive(_active); // set the object active
	}

	/// <summary>
	/// Opens the URL
	/// </summary>
	/// <param name="_url">URL</param>
	public void OpenURL(string _url)
	{
		Application.OpenURL(_url);
	}

	/// <summary>
	/// Update this instance
	/// </summary>
	protected void Update()
	{
		if (!_paused)
		{
			Time.timeScale = Mathf.Lerp(Time.timeScale, _cachedTimeScale, _timeScaleChangeDelta);
		}
	}
}	
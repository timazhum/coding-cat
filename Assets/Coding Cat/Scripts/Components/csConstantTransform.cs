﻿/* 
 * Name: csConstantTransformation.cs
 * Description: a component of a constant transformation
 * Author: Temirlan Zhumanov
 * Date: 23.04.2016
 * Email: lebrokholic@gmail.com
 * Copyright: All Rights Reserved.
 */

using UnityEngine;
using System.Collections;

public class csConstantTransform: csComponent
{
	/// <summary>
	/// Should the position be changed or not
	/// </summary>
	public bool _usePositionTransformation = false;

	/// <summary>
	/// Should the rotation be changed or not
	/// </summary>
	public bool _useRotationTransformation = false;

	/// <summary>
	/// Applying transformation of a position
	/// </summary>
	public Vector3 _positionTransformation = Vector3.zero;

	/// <summary>
	/// Applying transformation of a rotation
	/// </summary>
	public Vector3 _rotationTransformation = Vector3.zero;

	/// <summary>
	/// Should the time scale affect the transformation or not
	/// </summary>
	public bool _useTimeScale = true;

	/// <summary>
	/// Should the movement be perform with rigidbody or not
	/// </summary>
	public bool _useRigidbodyMovement = false;

	/// <summary>
	/// Chached transform of this object
	/// </summary>
	private Transform _transform = null;

	/// <summary>
	/// Chached rigidbody of this object
	/// </summary>
	private Rigidbody _rigidbody = null;

	/// <summary>
	/// Last update time of the component
	/// </summary>
	private float _updateLast = 0f;

	/// <summary>
	/// Start this instance
	/// </summary>
	protected void Start()
	{
		_transform = transform; // cache the transform of this object
		_rigidbody = GetComponent<Rigidbody>(); // cache the rigidbody of this object
	}

	/// <summary>
	/// Update this instance
	/// </summary>
	protected void FixedUpdate()
	{
		if (!_useRigidbodyMovement) UpdateTransform(); // if not using a rigidbody, then smooth the transform
		else UpdateRigidbody(); // otherwise, smooth the rigodbody

		_updateLast = Time.realtimeSinceStartup; // remember the last update time of the script
	}

	/// <summary>
	/// The function for the smooth transformation
	/// </summary>
	protected void UpdateTransform()
	{
		if (_transform) // if the transform exist
		{
			// Apply transformation to the position if allowed (with or without time scale effect)
			if (_usePositionTransformation) _transform.position += _useTimeScale?_positionTransformation * Time.deltaTime:
					_positionTransformation * (Time.realtimeSinceStartup - _updateLast);
			// Apply transformation to the rotation if allowed (with or without time scale effect)
			if (_useRotationTransformation) _transform.eulerAngles += _useTimeScale?_rotationTransformation * Time.deltaTime:
					_rotationTransformation * (Time.realtimeSinceStartup - _updateLast);
		}
	}

	/// <summary>
	/// The function for the smooth transformation
	/// </summary>
	protected void UpdateRigidbody()
	{
		if (_rigidbody) // if the transform exist
		{
			// Apply transformation to the position if allowed (with or without time scale effect)
			if (_usePositionTransformation) _rigidbody.MovePosition(_rigidbody.position + _positionTransformation *
				(_useTimeScale?Time.deltaTime:(Time.realtimeSinceStartup - _updateLast)));
			// Apply transformation to the rotation if allowed (with or without time scale effect)
			if (_useRotationTransformation) _rigidbody.MoveRotation(_rigidbody.rotation * Quaternion.Euler(_rotationTransformation *
				(_useTimeScale?Time.deltaTime:(Time.realtimeSinceStartup - _updateLast))));
		}
	}
}

﻿/* 
 * Name: csInput.cs
 * Description: a component of an input
 * Author: Temirlan Zhumanov
 * Date: 23.04.2016
 * Email: lebrokholic@gmail.com
 * Copyright: All Rights Reserved.
 */

using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class csInput: csSensor
{	
	/// <summary>
	/// Start this instance
	/// </summary>
	protected void Start()
	{
		AddRecognizers(); // add the recognizers
	}

	/// <summary>
	/// The const of a pinch argument
	/// </summary>
	public const string ARGUMENT_PINCH = "pinch";

	/// <summary>
	/// The const of a pan argument
	/// </summary>
	public const string ARGUMENT_PAN = "pan";

	/// <summary>
	/// The const of a tap collider argument
	/// </summary>
	public const string ARGUMENT_TAPCOLLIDER = "tapcollider";

	/// <summary>
	/// The const of a tap collider argument
	/// </summary>
	public const string ARGUMENT_TAPPOSITION = "tapposition";

	/// <summary>
	/// Adds the recognizers of input
	/// </summary>
	protected void AddRecognizers()
	{
		// The recognizer of a pinch gesture
		TKPinchRecognizer _pinchRecognizer = new TKPinchRecognizer();
		_pinchRecognizer.gestureRecognizedEvent += ( r ) =>
			NotifyListeners(new csArgumentsBuilder().
				Add(ARGUMENT_PINCH, _pinchRecognizer.deltaScale).Build());
		TouchKit.addGestureRecognizer(_pinchRecognizer);
		
		// The recognizer of a pan gesture
		TKPanRecognizer _panRecognizer = new TKPanRecognizer();		
		if(Application.platform == RuntimePlatform.IPhonePlayer)
			_panRecognizer.minimumNumberOfTouches = 2;		
		_panRecognizer.gestureRecognizedEvent += ( r ) =>
			NotifyListeners(new csArgumentsBuilder().
				Add(ARGUMENT_PAN, _panRecognizer.deltaTranslation).Build());
		TouchKit.addGestureRecognizer(_panRecognizer);
		
		// The recognizer of a tap gesture
		TKTapRecognizer _tapRecognizer = new TKTapRecognizer();
		_tapRecognizer.gestureRecognizedEvent += ( r ) =>
		{
			Vector2 _startTouchLocation = _tapRecognizer.startTouchLocation();
			Ray _cursorRay = Camera.main.ScreenPointToRay(
				new Vector3(_startTouchLocation.x, _startTouchLocation.y, 0f));
			RaycastHit _hit;

			if(Physics.Raycast(_cursorRay, out _hit, 1000.0f))
			{
				NotifyListeners(new csArgumentsBuilder().
					Add(ARGUMENT_TAPCOLLIDER, _hit.collider).
					Add(ARGUMENT_TAPPOSITION, _hit.point).Build());
			}
		};
		TouchKit.addGestureRecognizer(_tapRecognizer);
	}
}

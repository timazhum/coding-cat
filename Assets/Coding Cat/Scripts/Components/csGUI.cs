﻿/* 
 * Name: csGUI.cs
 * Description: a component of a GUI
 * Author: Temirlan Zhumanov
 * Date: 23.04.2016
 * Email: lebrokholic@gmail.com
 * Copyright: All Rights Reserved.
 */

using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;

public class csGUI: csComponent
{
	/// <summary>
	/// The set of all the activities in the application
	/// </summary>
	public csActivity[] _activities = null;

	/// <summary>
	/// The initial activities
	/// </summary>
	public csActivity[] _activitiesStackInitial = null;

	/// <summary>
	/// The stack of the activities
	/// </summary>
	private Stack<csActivity> _activitiesStack = new Stack<csActivity>();

	/// <summary>
	/// The object of a curtain
	/// </summary>
	public Image _curtain = null;

	/// <summary>
	/// The maximum step for the curtain show/hide
	/// </summary>
	public const int CURTAIN_MAX_STEP = 10;

	/// <summary>
	/// The delay for the curtain show/hide step
	/// </summary>
	public const float CURTAIN_DELAY = 0.05f;

	/// <summary>
	/// The object of a flash
	/// </summary>
	public Image _flash = null;

	/// <summary>
	/// The maximum step for the flash
	/// </summary>
	public const int FLASH_MAX_STEP = 10;

	/// <summary>
	/// The delay for the flash
	/// </summary>
	public const float FLASH_DELAY = 0.02f;

	/// <summary>
	/// Start this instance
	/// </summary>
	protected void Start()
	{
		if (_activitiesStackInitial != null) // if the stack of the activities exists
			for (int i = 0; i<_activitiesStackInitial.Length; i++) // fill it with the initial activities
				_activitiesStack.Push(_activitiesStackInitial[i]);
	}

	/// <summary>
	/// Opens the specific activity
	/// </summary>
	/// <param name="_activity">The activity</param>
	public void OpenActivity(csActivity _activity)
	{
		csActivity _tempActivity = GetCurrentActivity(); // remember the current activity
		PushActivityToStack(_activity); // add the opening activity to the stack
		SetActivityActive(_tempActivity, false); // disable the remembered activity
		SetActivityActive(GetCurrentActivity(), true); // enable the opening activity
	}

	/// <summary>
	/// Opens the specific activity by the name
	/// </summary>
	/// <param name="_activityName">The name of an activity</param>
	public void OpenActivity(string _activityName)
	{
		OpenActivity(GetActivityByName(_activityName));
	}

	/// <summary>
	/// Closes the activity
	/// </summary>
	public void CloseActivity()
	{
		csActivity _tempActivity = GetCurrentActivity(); // remember the closing activity
		PopActivityFromStack(); // removes the closing activity from the stack
		SetActivityActive(_tempActivity, false); // disable the closing activity
		SetActivityActive(GetCurrentActivity(), true); // enable the remaining activity
	}

	/// <summary>
	/// Closes the specific activity
	/// </summary>
	/// <param name="_activity">The activity</param>
	public void CloseActivity(csActivity _activity)
	{
		if (GetCurrentActivity().Equals(_activity)) CloseActivity(); // close the specific activity
	}

	/// <summary>
	/// Closes the specific activity
	/// </summary>
	/// <param name="_activityName">The name of the activity</param>
	public void CloseActivity(string _activityName)
	{
		CloseActivity(GetActivityByName(_activityName)); // close the current activity
	}

	/// <summary>
	/// Closes all the activities
	/// </summary>
	public void CloseAllActivities()
	{
		while (GetCurrentActivity() != null) CloseActivity(); // close all the activities
	}

	/// <summary>
	/// Sets the specific activity active
	/// </summary>
	/// <param name="_activity">The activity</param>
	/// <param name="_value">If set to <c>true</c> then activates, otherwise, deactivates</param>
	private void SetActivityActive(csActivity _activity, bool _value)
	{
		if (_activity) _activity.SetActive(_value); // set the specific activity active
	}

	/// <summary>
	/// Pushes the specific activity to the stack
	/// </summary>
	/// <param name="_activity">The activity</param>
	private void PushActivityToStack(csActivity _activity)
	{
		_activitiesStack.Push(_activity); // push the activity to the sack
	}

	/// <summary>
	/// Pops the activity from the stack
	/// </summary>
	/// <returns>The popped activity</returns>
	private csActivity PopActivityFromStack()
	{
		return _activitiesStack.Count>0?_activitiesStack.Pop():null; // pop the activity from the stack
	}

	/// <summary>
	/// Returns the peek activity (current one) in the stack of activities
	/// </summary>
	/// <returns>The current activity</returns>
	public csActivity GetCurrentActivity()
	{
		return _activitiesStack.Count>0?_activitiesStack.Peek():null; // return the current activity
	}

	/// <summary>
	/// Returns the name of the current activity
	/// </summary>
	/// <returns>The name of the current activity</returns>
	public string GetCurrentActivityName()
	{
		csActivity _activity = GetCurrentActivity(); // get the current activity
		return _activity != null?_activity.GetName():""; // return its name
	}

	/// <summary>
	/// Returns the activity by its name
	/// </summary>
	/// <returns>The activity by itsm name</returns>
	/// <param name="_activityName">The name of the activity</param>
	public csActivity GetActivityByName(string _activityName)
	{
		if (_activities != null) // if the array of activities is not null
			for (int i = 0; i<_activities.Length; i++) // go through every activity in the array
				if (_activities[i].GetName().Equals(_activityName)) return _activities[i]; // and check whether an activity has the required name or not
		
		return null; // if not found, return null
	}

	/// <summary>
	/// Show or hide the curtain
	/// </summary>
	/// <param name="_show">If set to <c>true</c> then show; otherwise, hide</param>
	public void Curtain(bool _show)
	{
		StartCoroutine(CurtainHelper(_show)); // execute the helper
	}

	/// <summary>
	/// The helper for the curtain function
	/// </summary>
	/// <param name="_show">If set to <c>true</c> then show the curtain; otherwise, hide</param>
	private IEnumerator CurtainHelper(bool _show)
	{
		if (_curtain) // if the curtain is assigned
		{
			int _step = _show?0:CURTAIN_MAX_STEP; // calculate the initial step

			_curtain.gameObject.SetActive(true); // set the curtain active

			while (_step != (_show?CURTAIN_MAX_STEP:0)) // iterate through every step
			{
				_curtain.color = new Color(_curtain.color.r, _curtain.color.g, _curtain.color.b,
				                           (float)_step / (float)CURTAIN_MAX_STEP); // assign a color
				_step += _show?1:-1; // change the step

				yield return StartCoroutine(csApplication.WaitForRealSeconds(CURTAIN_DELAY)); // wait for a specific time
			}

			_curtain.color = new Color(_curtain.color.r, _curtain.color.g, _curtain.color.b, _show?1f:0f); // assign the final color

			if (!_show) _curtain.gameObject.SetActive(false); // set the curtain inactive if it is required
		}
	}

	/// <summary>
	/// Returns the transition time of the curtain
	/// </summary>
	/// <returns>The transition time of the curtain</returns>
	public static float GetCurtainTransitionTime()
	{
		return CURTAIN_DELAY * CURTAIN_MAX_STEP + 0.5f; // return the transition time of the curtain
	}

	/// <summary>
	/// Show the flash
	/// </summary>
	public void Flash()
	{
		StopCoroutine(FlashHelper()); // stop the previous helper if is executed

		StartCoroutine(FlashHelper()); // execute the helper
	}

	/// <summary>
	/// The helper for the flash function
	/// </summary>
	private IEnumerator FlashHelper()
	{
		if (_flash) // if the flash is assigned
		{			
			_flash.gameObject.SetActive(true); // set the flash active

			for (int i = 0; i < FLASH_MAX_STEP; i++) // iterate through every step
			{
				_flash.color = new Color(_flash.color.r, _flash.color.g, _flash.color.b,
					(float)(FLASH_MAX_STEP - i) / (float)FLASH_MAX_STEP); // assign a color

				yield return StartCoroutine(csApplication.WaitForRealSeconds(FLASH_DELAY)); // wait for a specific time
			}
				
			_flash.color = new Color(_flash.color.r, _flash.color.g, _flash.color.b, 0f); // assign the final color

			_flash.gameObject.SetActive(false); // set the flash inactive
		}
	}

	/// <summary>
	/// Invokes the on click event
	/// </summary>
	/// <param name="_button">A button</param>
	public void InvokeOnClick(Button _button)
	{
		if (_button) _button.onClick.Invoke(); // invoke the method
	}		
}

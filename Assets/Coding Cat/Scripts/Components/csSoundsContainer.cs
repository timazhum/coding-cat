﻿/* 
 * Name: csSoundsContainer.cs
 * Description: a component of the sounds container
 * Author: Temirlan Zhumanov
 * Date: 23.04.2016
 * Email: lebrokholic@gmail.com
 * Copyright: All Rights Reserved.
 */

using UnityEngine;
using System.Collections;

public class csSoundsContainer: csSingletonComponent
{
	/// <summary>
	/// The mute state of the sounds
	/// </summary>
	private bool _mute = false;

	/// <summary>
	/// The pitch of the sounds
	/// </summary>
	private float _pitch = -1f;

	/// <summary>
	/// The array of sounds
	/// </summary>
	public AudioSource[] _sounds = null;

	/// <summary>
	/// The array which reveals whether the sound should be reinitialized or not
	/// </summary>
	public bool[] _reinitializeOnStart = null;

	/// <summary>
	/// The cached instance of the application
	/// </summary>
	private csApplication _application = null;

	/// <summary>
	/// Raised when the level is loaded
	/// </summary>
	/// <param name="_level">The index of a level</param>
	protected void OnLevelWasLoaded(int _level)
	{
		_application = csClassFetcher<csApplication>.GetInstance();

		if (_sounds == null || _reinitializeOnStart == null) return; // return if the arrays are not initialized
		if (_sounds.Length != _reinitializeOnStart.Length) return; // returns if the sizes are not identical

		for (int i = 0; i < _sounds.Length; i++) // for every sound
			if (_reinitializeOnStart[i] && _sounds[i] != null) // if it should be reinitialized
				_sounds[i].Stop(); // then perform the reinitialization
	}

	/// <summary>
	/// Play the specified sound
	/// </summary>
	/// <param name="_name">The name of a sound's source to play</param>
	public void Play(string _name)
	{
		AudioSource _sound = null; // the audio source to be played

		if (_sounds != null) // if the array of audio sources is not null
			for (int i = 0; i < _sounds.Length; i++) // go through every audio source
				if (_sounds[i] && _sounds[i].name.Equals(_name)) _sound = _sounds[i]; // and check whether it is the required one or not

		if (_sound) _sound.Play(); // play the audio source
	}

	/// <summary>
	/// Pause the specified sound
	/// </summary>
	/// <param name="_name">The name of a sound's source to pause</param>
	public void Pause(string _name)
	{
		AudioSource _sound = null; // the audio source to be played

		if (_sounds != null) // if the array of audio sources is not null
			for (int i = 0; i < _sounds.Length; i++) // go through every audio source
				if (_sounds[i] && _sounds[i].name.Equals(_name)) _sound = _sounds[i]; // and check whether it is the required one or not

		if (_sound) _sound.Pause(); // pause the audio source
	}

	/// <summary>
	/// Stop the specified sound
	/// </summary>
	/// <param name="_name">The name of a sound's source to stop</param>
	public void Stop(string _name)
	{
		AudioSource _sound = null; // the audio source to be played

		if (_sounds != null) // if the array of audio sources is not null
			for (int i = 0; i < _sounds.Length; i++) // go through every audio source
				if (_sounds[i] && _sounds[i].name.Equals(_name)) _sound = _sounds[i]; // and check whether it is the required one or not

		if (_sound) _sound.Stop(); // stop the audio source
	}

	/// <summary>
	/// Mute or unmute the sounds
	/// </summary>
	/// <param name="_state">If set to <c>true</c> then, mute; otherwise, unmute</param>
	public void Mute(bool _state)
	{
		_mute = _state; // remember the current state of the mute

		if (_sounds != null) // if the array of audio sources is not null
			for (int i = 0; i < _sounds.Length; i++) // go through every audio source
				if (_sounds[i])
					_sounds[i].mute = _mute; // assign the current state to the mute value of an audio source
	}

	/// <summary>
	/// Determines whether the sound is mute or not
	/// </summary>
	/// <returns><c>true</c> if the sound is mute; otherwise, <c>false</c>.</returns>
	public bool IsMute()
	{
		return _mute;
	}		

	/// <summary>
	/// Start this instance
	/// </summary>
	protected void Start()
	{
		_application = csClassFetcher<csApplication>.GetInstance();

		LoadData(); // load the data
	}

	/// <summary>
	/// Raised when the application is quitting
	/// </summary>
	protected void OnApplicationQuit()
	{
		SaveData();
	}

	/// <summary>
	/// Raised when the object is destroyed
	/// </summary>
	protected override void OnDestroy()
	{
		base.OnDestroy();

		SaveData();
	}

	/// <summary>
	/// Loads the data
	/// </summary>
	public void LoadData()
	{
		if (PlayerPrefs.GetInt("_mute", 0) == 1) Mute(true);
		else Mute(false);
	}

	/// <summary>
	/// Saves the data
	/// </summary>
	public void SaveData()
	{		
		if (_mute) PlayerPrefs.SetInt("_mute", 1);
		else PlayerPrefs.SetInt("_mute", 0);
	}
		
	/// <summary>
	/// Update this instance
	/// </summary>
	protected void Update()
	{
		float _temp = _pitch;

		if (_application)
		{
			_pitch = _application.IsPaused() ? 1f : _application.GetCachedTimeScale();
		}

		if (!Mathf.Approximately(_temp,_pitch))
			for (int i = 0; i < _sounds.Length; i++)
				if (_sounds[i])
					_sounds[i].pitch = _pitch;
	}
}

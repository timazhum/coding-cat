﻿/* 
 * Name: csMoneyPrize.cs
 * Description: a component of a money prize
 * Author: Temirlan Zhumanov
 * Date: 13.05.2016
 * Email: lebrokholic@gmail.com
 * Copyright: All Rights Reserved.
 */

using UnityEngine;
using System.Collections;

public class csMoneyPrize: csPrize
{
	/// <summary>
	/// The amount of a money to reward
	/// </summary>
	public int _amount = 0;

	/// <summary>
	/// Reward the player
	/// </summary>
	public override void Reward()
	{
		csPlayer _player = csClassFetcher<csPlayer>.GetInstance();
		if (_player)
			_player.AddMoneyAmount(_amount);

		csGUI _gui = csClassFetcher<csGUI>.GetInstance();
		csActivityPrize _activityPrize = _gui ? _gui.GetActivityByName(csActivityPrize.NAME) as csActivityPrize : null;
		if (_activityPrize)
		{
			_activityPrize.UpdateUI(new csArgumentsBuilder().
				Add(csActivityPrize.ARGUMENT_DESCRIPTION, "$" + _amount.ToString()).
				Add(csActivityPrize.ARGUMENT_IMAGE, _image).Build());
		}
	}
}
﻿/* 
 * Name: csAttachToTransform.cs
 * Description: a component of a transform attachment
 * Author: Temirlan Zhumanov
 * Date: 23.04.2016
 * Email: lebrokholic@gmail.com
 * Copyright: All Rights Reserved.
 */

using UnityEngine;
using System.Collections;

public class csAttachToTransform: csComponent
{
	/// <summary>
	/// The target of the transform
	/// </summary>
	public Transform _target = null;

	/// <summary>
	/// The displacement from the target
	/// </summary>
	public Vector3 _displacement = Vector3.zero;

	/// <summary>
	/// The cached transform
	/// </summary>
	private Transform _transform = null;

	/// <summary>
	/// The cached rigidbody
	/// </summary>
	private Rigidbody _rigidbody = null;

	/// <summary>
	/// The desired position of the transform
	/// </summary>
	private Vector3 _desiredPosition = Vector3.zero;

	/// <summary>
	/// The desired rotation of the transform
	/// </summary>
	private Quaternion _desiredRotation = Quaternion.identity;

	/// <summary>
	/// Should the position be changed or not
	/// </summary>
	public bool _canChangePosition = true;

	/// <summary>
	/// Should the rotation be changed or not
	/// </summary>
	public bool _canChangeRotation = true;

	/// <summary>
	/// Should the movement be smooth or not
	/// </summary>
	public bool _useSmoothMovement = true;

	/// <summary>
	/// Should the movement be perform with rigidbody or not
	/// </summary>
	public bool _useRigidbodyMovement = false;

	/// <summary>
	/// The smooth movement coefficient for the position change
	/// </summary>
	public float _smoothPositionDelta = 10f;

	/// <summary>
	/// The smooth movement coefficient for the rotation change
	/// </summary>
	public float _smoothRotationDelta = 10f;

	/// <summary>
	/// Start this instance
	/// </summary>
	protected void Start()
	{
		Initialize();
	}

	/// <summary>
	/// Initialize this instance
	/// </summary>
	protected void Initialize()
	{
		_transform = transform; // cache the transform
		_rigidbody = GetComponent<Rigidbody>(); // cache the rigidbody
	}

	/// <summary>
	/// Called every update of the physics
	/// </summary>
	protected void FixedUpdate()
	{
		if (_transform && _target) // if the transform and target exist
		{
			_desiredPosition = _target.TransformPoint(_displacement); // update the desired position
			_desiredRotation = _target.rotation; // update the desired rotation
		}

		if (!_useRigidbodyMovement) SmoothTransform(); // if not using a rigidbody, then smooth the transform
		else SmoothRigidbody(); // otherwise, smooth the rigodbody
	}

	/// <summary>
	/// The function for the smooth transformation
	/// </summary>
	protected void SmoothTransform()
	{
		if (_transform) // if the transform exist
		{
			// Update the position if it is allowed
			if (_canChangePosition) _transform.position = _useSmoothMovement?Vector3.Lerp(_transform.position, _desiredPosition, _smoothPositionDelta * Time.deltaTime):_desiredPosition;
			// Update the rotation if it is allowed
			if (_canChangeRotation) _transform.rotation = _useSmoothMovement?Quaternion.Lerp(_transform.rotation, _desiredRotation, _smoothRotationDelta * Time.deltaTime):_desiredRotation;
		}
	}

	/// <summary>
	/// The function for the smooth transformation
	/// </summary>
	protected void SmoothRigidbody()
	{
		if (_rigidbody) // if the transform exist
		{
			// Update the position if it is allowed
			if (_canChangePosition) _rigidbody.MovePosition(_useSmoothMovement?Vector3.Lerp(_rigidbody.position, _desiredPosition, _smoothPositionDelta * Time.deltaTime):_desiredPosition);
			// Update the rotation if it is allowed
			if (_canChangeRotation) _rigidbody.MoveRotation(_useSmoothMovement?Quaternion.Lerp(_rigidbody.rotation, _desiredRotation, _smoothRotationDelta * Time.deltaTime):_desiredRotation);
		}
	}
}
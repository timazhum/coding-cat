﻿/* 
 * Name: csCamera.cs
 * Description: a component of a camera
 * Author: Temirlan Zhumanov
 * Date: 23.04.2016
 * Email: lebrokholic@gmail.com
 * Copyright: All Rights Reserved.
 */

using UnityEngine;
using System.Collections;

public class csCamera: csListener
{
	/// <summary>
	/// The target of a camera attachment
	/// </summary>
	public Transform _cameraTarget = null;

	/// <summary>
	/// The speed of camera's movement
	/// </summary>
	public float _cameraMovementSpeed = 1f;

	/// <summary>
	/// The cached GUI
	/// </summary>
	private csGUI _gui = null;

	/// <summary>
	/// The GUI reflection
	/// </summary>
	private csGUI _GUI { get { return _gui ?? (_gui = csClassFetcher<csGUI>.GetInstance()); } }

	/// <summary>
	/// Start this instance
	/// </summary>
	protected void Start()
	{
		csInput _input = csClassFetcher<csInput>.GetInstance();
		if (_input) _input.AddListener(this);		
	}

	/// <summary>
	/// Updates the listener
	/// </summary>
	/// <param name="_arguments">Array of arguments</param>
	public override void UpdateListener(object[] _arguments)
	{		
		if (_GUI == null || !csActivityMain.NAME.Equals(_GUI.GetCurrentActivityName())) return;

		csArgumentsReader _argumentsReader = new csArgumentsReader(_arguments);

		if (_argumentsReader != null)
		{
			if (_argumentsReader.HasKey(csInput.ARGUMENT_PAN))
			{
				object _value = _argumentsReader.GetValue(csInput.ARGUMENT_PAN);
				if (_value is Vector2 && _cameraTarget)
				{
					Vector2 _vector2 = (Vector2)_value;
					_cameraTarget.position -= (Vector3.right * _vector2.x + Vector3.up * _vector2.y) * _cameraMovementSpeed;
				}
			}

			if (_argumentsReader.HasKey(csInput.ARGUMENT_PINCH))
			{
				object _value = _argumentsReader.GetValue(csInput.ARGUMENT_PINCH);
				if (_value is float)
				{
					float _float = (float)_value;

					// TODO: Control the camera's field of view
				}
			}
		}
	}

	/// <summary>
	/// Update this instance
	/// </summary>
	protected void Update()
	{

	}		
}
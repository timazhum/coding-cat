﻿/* 
 * Name: csCharacter.cs
 * Description: a component of a character
 * Author: Temirlan Zhumanov
 * Date: 24.04.2016
 * Email: lebrokholic@gmail.com
 * Copyright: All Rights Reserved.
 */

using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;
using System.Collections;

public class csCharacter: csComponent
{
	/// <summary>
	/// The identifier of a character
	/// </summary>
	public string _id = "";

	/// <summary>
	/// The navigation mesh agent
	/// </summary>
	public NavMeshAgent _navMeshAgent = null;

	/// <summary>
	/// The graphics of a character
	/// </summary>
	public Transform _graphics = null;

	/// <summary>
	/// The animator of a character
	/// </summary>
	public Animator _animator = null;

	/// <summary>
	/// The name of a character
	/// </summary>
	public string _name = "";

	/// <summary>
	/// The life amount
	/// </summary>
	protected float _lifeAmount
	{
		get
		{
			return PlayerPrefs.GetFloat("lifeamount_" + _id, 1f);
		}
		set
		{
			PlayerPrefs.SetFloat("lifeamount_" + _id, value);
			PlayerPrefs.Save();
		}
	}

	/// <summary>
	/// The energy amount
	/// </summary>
	protected float _energyAmount
	{
		get
		{
			return PlayerPrefs.GetFloat("energyamount_" + _id, 1f);
		}
		set
		{
			PlayerPrefs.SetFloat("energyamount_" + _id, value);
			PlayerPrefs.Save();
		}
	}

	/// <summary>
	/// The animator parameter of speed
	/// </summary>
	public const string ANIMATORPARAMETER_SPEED = "speed";

	/// <summary>
	/// Heal the character
	/// </summary>
	/// <param name="_amount">Amount of heal</param>
	public virtual void Heal(float _amount)
	{
		ChangeLifeAmount(_amount);
	}

	/// <summary>
	/// Hurt the character
	/// </summary>
	/// <param name="_amount">Amount of hurt</param>
	public virtual void Hurt(float _amount)
	{
		ChangeLifeAmount(-_amount);
	}

	/// <summary>
	/// Changes the life amount
	/// </summary>
	/// <param name="_delta">Delta value</param>
	protected void ChangeLifeAmount(float _delta)
	{
		_lifeAmount = Mathf.Clamp(_lifeAmount + _delta, 0f, 1f);
	}

	/// <summary>
	/// Charge the character
	/// </summary>
	/// <param name="_amount">Amount of charge</param>
	public virtual void Charge(float _amount)
	{
		ChangeEnergyAmount(_amount);
	}

	/// <summary>
	/// Discharge the character
	/// </summary>
	/// <param name="_amount">Amount of discharge</param>
	public virtual void Discharge(float _amount)
	{
		ChangeEnergyAmount(-_amount);
	}

	/// <summary>
	/// Changes the energy amount
	/// </summary>
	/// <param name="_delta">Delta value</param>
	protected void ChangeEnergyAmount(float _delta)
	{
		_energyAmount = Mathf.Clamp(_energyAmount + _delta, 0f, 1f);
	}

	/// <summary>
	/// Move the character to a position
	/// </summary>
	/// <param name="_position">Position to move</param>
	public virtual void Move(Vector3 _position)
	{
		if (_navMeshAgent)
			_navMeshAgent.destination = _position;
	}

	/// <summary>
	/// Gets the life amount
	/// </summary>
	/// <returns>The life amount</returns>
	public float GetLifeAmount()
	{
		return _lifeAmount;
	}

	/// <summary>
	/// Gets the energy amount
	/// </summary>
	/// <returns>The energy amount</returns>
	public float GetEnergyAmount()
	{
		return _energyAmount;
	}

	/// <summary>
	/// Update this instance
	/// </summary>
	protected void Update()
	{
		// Modify the graphics rotation
		if (_graphics)
		{
			_graphics.rotation = Quaternion.identity;
			if (transform.eulerAngles.y >= 0f && transform.eulerAngles.y <= 180f) _graphics.Rotate(Vector3.up * 180f);				
		}

		// Modify the graphics animation
		if (_navMeshAgent && _animator)
			_animator.SetFloat(ANIMATORPARAMETER_SPEED, _navMeshAgent.velocity.magnitude);
	}
}

﻿/* 
 * Name: csSoundsPlaybackController.cs
 * Description: a component of a sounds playback controller
 * Author: Temirlan Zhumanov
 * Date: 23.04.2016
 * Email: lebrokholic@gmail.com
 * Copyright: All Rights Reserved.
 */

using UnityEngine;
using System.Collections;

public class csSoundsPlaybackController: csComponent
{
	/// <summary>
	/// The cached instance of the sounds container
	/// </summary>
	private csSoundsContainer _soundsContainer = null;

	/// <summary>
	/// Start this instance
	/// </summary>
	protected void Start()
	{
		_soundsContainer = csClassFetcher<csSoundsContainer>.GetInstance(); // cache the GUI instance
	}

	/// <summary>
	/// Mutes or unmutes the sound
	/// </summary>
	/// <param name="_mute">If set to <c>true</c>, then mutes</param>
	public void MuteSound(bool _mute)
	{
		if (_soundsContainer) _soundsContainer.Mute(_mute);
	}

	/// <summary>
	/// Plays the sound
	/// </summary>
	/// <param name="_name">Name of the sound</param>
	public void PlaySound(string _name)
	{
		if (_soundsContainer) _soundsContainer.Play(_name);
	}

	/// <summary>
	/// Stops the sound
	/// </summary>
	/// <param name="_name">Name of the sound</param>
	public void StopSound(string _name)
	{
		if (_soundsContainer) _soundsContainer.Stop(_name);
	}

	/// <summary>
	/// Pause the sound
	/// </summary>
	/// <param name="_name">Name of the sound</param>
	public void PauseSound(string _name)
	{
		if (_soundsContainer) _soundsContainer.Pause(_name);
	}
}

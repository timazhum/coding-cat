﻿/* 
 * Name: csActivityGame.cs
 * Description: an activity of the game screen
 * Author: Temirlan Zhumanov
 * Date: 24.04.2016
 * Email: lebrokholic@gmail.com
 * Copyright: All Rights Reserved.
 */

using UnityEngine;
using UnityEngine.UI;
using UnityEngine.UI.Extensions;
using System.Collections;
using System.Collections.Generic;

public class csActivityGame: csActivity
{
	/// <summary>
	/// The name of the activity
	/// </summary>
	public const string NAME = "activityGame";

	/// <summary>
	/// Gets the name of the activity's instance
	/// </summary>
	/// <returns>The name of the activity's instance</returns>
	public override string GetName()
	{
		return NAME;
	}

	/// <summary>
	/// Updates the UI
	/// </summary>
	public override void UpdateUI(object[] _arguments)
	{
		
	}

	/// <summary>
	/// The views
	/// </summary>
	public List<csView> _views = new List<csView>();

	/// <summary>
	/// Opens the view
	/// </summary>
	/// <param name="_view">View</param>
	public void OpenView(csView _view)
	{
		if (_views != null)
			for (int i = 0; i < _views.Count; i++)
				if (_views[i] && _views[i].gameObject)
					_views[i].gameObject.SetActive(_views[i].Equals(_view));
	}

	/// <summary>
	/// Closes the views
	/// </summary>
	public void CloseViews()
	{
		if (_views != null)
			for (int i = 0; i < _views.Count; i++)
				if (_views[i] && _views[i].gameObject)
					_views[i].gameObject.SetActive(false);
	}
}

﻿/* 
 * Name: csActivityMain.cs
 * Description: an activity of the main screen
 * Author: Temirlan Zhumanov
 * Date: 24.04.2016
 * Email: lebrokholic@gmail.com
 * Copyright: All Rights Reserved.
 */

using UnityEngine;
using UnityEngine.UI;
using UnityEngine.UI.Extensions;
using System.Collections;
using System.Collections.Generic;

public class csActivityMain: csActivity
{
	/// <summary>
	/// The name of the activity
	/// </summary>
	public const string NAME = "activityMain";

	/// <summary>
	/// Gets the name of the activity's instance
	/// </summary>
	/// <returns>The name of the activity's instance</returns>
	public override string GetName()
	{
		return NAME;
	}

	/// <summary>
	/// The progress bar of life amount
	/// </summary>
	public Slider _progressBarLifeAmount = null;

	/// <summary>
	/// The progress bar of energy amount
	/// </summary>
	public Slider _progressBarEnergyAmount = null;

	/// <summary>
	/// The name of the character
	/// </summary>
	public Text _textCharacterName = null;

	/// <summary>
	/// The amount of money
	/// </summary>
	public Text _textMoneyAmount = null;

	/// <summary>
	/// The argument of life amount
	/// </summary>
	public const string ARGUMENT_LIFEAMOUNT = "life";

	/// <summary>
	/// The argument of energy amount
	/// </summary>
	public const string ARGUMENT_ENERGYAMOUNT = "energy";

	/// <summary>
	/// The argument of character name
	/// </summary>
	public const string ARGUMENT_CHARACTERNAME = "charactername";

	/// <summary>
	/// The argument of character name
	/// </summary>
	public const string ARGUMENT_MONEYAMOUNT = "moneyamount";

	/// <summary>
	/// Updates the UI
	/// </summary>
	public override void UpdateUI(object[] _arguments)
	{
		csArgumentsReader _argumentsReader = new csArgumentsReader(_arguments);

		if (_argumentsReader.HasKey(ARGUMENT_LIFEAMOUNT))
		{
			object _value = _argumentsReader.GetValue(ARGUMENT_LIFEAMOUNT);
			if (_value is float && _progressBarLifeAmount) _progressBarLifeAmount.value = (float)_value;
		}

		if (_argumentsReader.HasKey(ARGUMENT_ENERGYAMOUNT))
		{
			object _value = _argumentsReader.GetValue(ARGUMENT_ENERGYAMOUNT);
			if (_value is float && _progressBarEnergyAmount) _progressBarEnergyAmount.value = (float)_value;
		}

		if (_argumentsReader.HasKey(ARGUMENT_CHARACTERNAME))
		{
			object _value = _argumentsReader.GetValue(ARGUMENT_CHARACTERNAME);
			if (_value is string && _textCharacterName) _textCharacterName.text = (string)_value;
		}

		if (_argumentsReader.HasKey(ARGUMENT_MONEYAMOUNT))
		{
			object _value = _argumentsReader.GetValue(ARGUMENT_MONEYAMOUNT);
			if (_value is int && _textMoneyAmount) _textMoneyAmount.text = ((int)_value).ToString();
		}
	}
}

﻿/* 
 * Name: csActivityGamesList.cs
 * Description: an activity of the games list screen
 * Author: Temirlan Zhumanov
 * Date: 24.04.2016
 * Email: lebrokholic@gmail.com
 * Copyright: All Rights Reserved.
 */

using UnityEngine;
using UnityEngine.UI;
using UnityEngine.UI.Extensions;
using System.Collections;
using System.Collections.Generic;

public class csActivityGamesList: csActivity
{
	/// <summary>
	/// The name of the activity
	/// </summary>
	public const string NAME = "activityGamesList";

	/// <summary>
	/// Gets the name of the activity's instance
	/// </summary>
	/// <returns>The name of the activity's instance</returns>
	public override string GetName()
	{
		return NAME;
	}

	/// <summary>
	/// Updates the UI
	/// </summary>
	public override void UpdateUI(object[] _arguments)
	{
		
	}

	/// <summary>
	/// The games
	/// </summary>
	public List<csGame> _games = new List<csGame>();

	/// <summary>
	/// Play the specific game
	/// </summary>
	/// <param name="_index">Index of the game</param>
	public void Play(int _index)
	{
		if (_games != null && _index >= 0 && _index < _games.Count && _games[_index])
			_games[_index].Play();
	}
}

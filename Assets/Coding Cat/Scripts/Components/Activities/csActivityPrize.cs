﻿/* 
 * Name: csActivityPrize.cs
 * Description: an activity of the prize screen
 * Author: Temirlan Zhumanov
 * Date: 24.04.2016
 * Email: lebrokholic@gmail.com
 * Copyright: All Rights Reserved.
 */

using UnityEngine;
using UnityEngine.UI;
using UnityEngine.UI.Extensions;
using System.Collections;
using System.Collections.Generic;

public class csActivityPrize: csActivity
{
	/// <summary>
	/// The name of the activity
	/// </summary>
	public const string NAME = "activityPrize";

	/// <summary>
	/// Gets the name of the activity's instance
	/// </summary>
	/// <returns>The name of the activity's instance</returns>
	public override string GetName()
	{
		return NAME;
	}

	/// <summary>
	/// The argument of description
	/// </summary>
	public const string ARGUMENT_DESCRIPTION = "description";

	/// <summary>
	/// The argument of image
	/// </summary>
	public const string ARGUMENT_IMAGE = "image";

	/// <summary>
	/// Updates the UI
	/// </summary>
	public override void UpdateUI(object[] _arguments)
	{
		csArgumentsReader _argumentsReader = new csArgumentsReader(_arguments);

		if (_argumentsReader.HasKey(ARGUMENT_DESCRIPTION))
		{
			object _value = _argumentsReader.GetValue(ARGUMENT_DESCRIPTION);
			if (_value is string && _textDescription) _textDescription.text = (string)_value;
		}

		if (_argumentsReader.HasKey(ARGUMENT_IMAGE))
		{
			object _value = _argumentsReader.GetValue(ARGUMENT_IMAGE);
			if (_value is Sprite && _imagePrize) _imagePrize.sprite = _value as Sprite;
		}
	}

	/// <summary>
	/// The text description of a prize
	/// </summary>
	public Text _textDescription = null;

	/// <summary>
	/// The image of a prize
	/// </summary>
	public Image _imagePrize = null;
}

﻿/* 
 * Name: csActivityShop.cs
 * Description: an activity of the shop screen
 * Author: Temirlan Zhumanov
 * Date: 24.04.2016
 * Email: lebrokholic@gmail.com
 * Copyright: All Rights Reserved.
 */

using UnityEngine;
using UnityEngine.UI;
using UnityEngine.UI.Extensions;
using System.Collections;
using System.Collections.Generic;

public class csActivityShop: csActivity
{
	/// <summary>
	/// The name of the activity
	/// </summary>
	public const string NAME = "activityShop";

	/// <summary>
	/// Gets the name of the activity's instance
	/// </summary>
	/// <returns>The name of the activity's instance</returns>
	public override string GetName()
	{
		return NAME;
	}

	/// <summary>
	/// Updates the UI
	/// </summary>
	public override void UpdateUI(object[] _arguments)
	{
		
	}
}

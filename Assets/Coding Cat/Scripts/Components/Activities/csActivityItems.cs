﻿/* 
 * Name: csActivityItems.cs
 * Description: an activity of the items screen
 * Author: Temirlan Zhumanov
 * Date: 24.04.2016
 * Email: lebrokholic@gmail.com
 * Copyright: All Rights Reserved.
 */

using UnityEngine;
using UnityEngine.UI;
using UnityEngine.UI.Extensions;
using System.Collections;
using System.Collections.Generic;

public class csActivityItems: csActivity
{
	/// <summary>
	/// The name of the activity
	/// </summary>
	public const string NAME = "activityItems";

	/// <summary>
	/// Gets the name of the activity's instance
	/// </summary>
	/// <returns>The name of the activity's instance</returns>
	public override string GetName()
	{
		return NAME;
	}

	/// <summary>
	/// Updates the UI
	/// </summary>
	public override void UpdateUI(object[] _arguments)
	{
		foreach (csViewActivityItemsItem _itemView in _itemsViews)
			if (_itemView && _itemView.gameObject)
				DestroyImmediate(_itemView.gameObject);

		if (_itemsViews != null) _itemsViews.Clear();

		foreach (csItem _item in _items)
			if (_item)
			{
				csViewActivityItemsItem _itemView = Instantiate(_prefabItemView) as csViewActivityItemsItem;
				if (_itemView)
				{					
					if (_itemView.transform)
					{
						_itemView.transform.SetParent(_itemsViewsContainer);
						_itemView.transform.localScale = Vector3.one;
					}

					if (_itemView.gameObject)
					{
						_itemView.gameObject.name = "itemView" + _item._name;
						_itemView.gameObject.SetActive(true);
					}

					_itemView._controller = _item;
					if (_item._views != null)
					{
						_item._views.Remove(_itemView);
						_item._views.Add(_itemView);
					}
					_item.UpdateViews();

					if (_itemsViews != null) _itemsViews.Add(_itemView);
				}					
			}
	}
			
	/// <summary>
	/// The items
	/// </summary>
	public List<csItem> _items = new List<csItem>();

	/// <summary>
	/// The prefab of item view
	/// </summary>
	public csViewActivityItemsItem _prefabItemView = null;

	/// <summary>
	/// The items views container
	/// </summary>
	public Transform _itemsViewsContainer = null;

	/// <summary>
	/// The views of items
	/// </summary>
	[HideInInspector]
	public List<csViewActivityItemsItem> _itemsViews = new List<csViewActivityItemsItem>();

	/// <summary>
	/// Start this instance
	/// </summary>
	protected void Start()
	{
		UpdateUI(null);
	}

	/// <summary>
	/// When the object is enabled
	/// </summary>
	protected void OnEnable()
	{
		UpdateUI(null);
	}
}
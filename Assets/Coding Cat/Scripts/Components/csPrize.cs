﻿/* 
 * Name: csPrize.cs
 * Description: a component of a prize
 * Author: Temirlan Zhumanov
 * Date: 13.05.2016
 * Email: lebrokholic@gmail.com
 * Copyright: All Rights Reserved.
 */

using UnityEngine;
using System.Collections;

public abstract class csPrize: csComponent
{
	/// <summary>
	/// The image of a prize
	/// </summary>
	public Sprite _image = null;

	/// <summary>
	/// Reward the player
	/// </summary>
	public abstract void Reward();
}
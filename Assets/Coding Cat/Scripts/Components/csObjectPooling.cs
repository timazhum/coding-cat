﻿/* 
 * Name: csObjectPooling.cs
 * Description: a component of an object pooling
 * Author: Temirlan Zhumanov
 * Date: 23.04.2016
 * Email: lebrokholic@gmail.com
 * Copyright: All Rights Reserved.
 */

using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;

public class csObjectPooling: csComponent
{	
	/// <summary>
	/// The number of unique objects
	/// </summary>
	public int _objectsNumber = 0;

	/// <summary>
	/// The prefabs of unique objects
	/// </summary>
	public GameObject[] _objectsPrefabs = null;

	/// <summary>
	/// The number of copies of each of the unique objects
	/// </summary>
	public int[] _objectsCopies = null;

	/// <summary>
	/// The cached transform of the object pooling instance
	/// </summary>
	private Transform _transform = null;

	/// <summary>
	/// The list of all the instances
	/// </summary>
	private List<GameObject> _instances = new List<GameObject>();

	/// <summary>
	/// The list of times when the instances were created
	/// </summary>
	private List<float> _instancesTime = new List<float>();

	/// <summary>
	/// The sorted dictionary from the name of an unique object to its index
	/// </summary>
	private SortedDictionary<string, int> _mappingToIndex = new SortedDictionary<string, int>();

	/// <summary>
	/// The list which maps to the index of the first instance of a specific unique object
	/// </summary>
	private List<int> _mappingToInstance = new List<int>();

	/// <summary>
	/// Start this instance
	/// </summary>
	protected void Start()
	{
		Initialize();
	}

	/// <summary>
	/// Initialize this instance
	/// </summary>
	public void Initialize()
	{
		_transform = transform; // cache the transform

		if (_objectsPrefabs == null || // return if the array of unique objects does not exist
			_objectsNumber != _objectsPrefabs.Length || // return if the array of unique objects does not have a specific length
			_objectsCopies == null || // return if the array of unique objects copies does not exist
			_instances == null || // return if the list of instances does not exist
			_instancesTime == null || // return if the list of instances time does not exist
			_mappingToIndex == null || // return if the sorted dictionary from the name of a unique object to its index does not exist
			_mappingToInstance == null) return; // return if the list which maps to the index of the first instance of a specific unique object does not exist

		for (int i = 0; i<_objectsNumber; i++) // for every unique object
		{
			if (_objectsPrefabs[i] == null) return; // return if the prefab does not exist

			_mappingToIndex.Add(_objectsPrefabs[i].name, i); // map its name to a specific index
			_mappingToInstance.Add(_instances.Count); // map its index to the index of a first instance
			
			for (int j = 0; j<_objectsCopies[i]; j++) // for every copy of the unique object
			{
				GameObject _go = Instantiate(_objectsPrefabs[i], Vector3.zero, Quaternion.identity) as GameObject; // create an instance
				if (_go == null) return; // if not created, then return

				_go.SetActive(false); // deactivate the instance
				_go.transform.parent = _transform; // set the position of the instance
				_go.transform.localScale = Vector3.one; // set the local scale of the instance
				
				_instances.Add(_go); // add the instance to the list of instances
				_instancesTime.Add(Time.time); // add the time to the list of instances time
			}
		}
	}

	/// <summary>
	/// Pops the instance
	/// </summary>
	/// <returns>The popped instance</returns>
	/// <param name="_name">Name of a unique object</param>
	/// <param name="_position">Position of an instance</param>
	public GameObject PopInstance(string _name, Vector3 _position)
	{
		GameObject _result = null; // the result of the function

		if (_objectsPrefabs == null || // return if the array of unique objects does not exist
			_objectsNumber != _objectsPrefabs.Length || // return if the array of unique objects does not have a specific length
			_objectsCopies == null || // return if the array of unique objects copies does not exist
			_instances == null || // return if the list of instances does not exist
			_instancesTime == null || // return if the list of instances time does not exist
			_mappingToIndex == null || // return if the sorted dictionary from the name of a unique object to its index does not exist
			_mappingToInstance == null) return null; // return if the list which maps to the index of the first instance of a specific unique object does not exist

		int _index; // index inside of the sorted dictionary
		if (!_mappingToIndex.TryGetValue(_name, out _index)) _index = -1; // try to receive the index
		
		if (_index >= 0 && _index < _mappingToInstance.Count && _index < _objectsCopies.Length) // check whether fits the sizes of the structures
		{
			int _instance = _mappingToInstance[_index]; // get the specific unique object's first instance index

			int _oldestInstanceIndex = -1; // the index of the oldest instance
			float _oldestInstanceTime = Mathf.Infinity; // the time of the oldest instance

			for (int i = _instance; i<_instance + _objectsCopies[_index]; i++) // for every instance of a specific unique object
			{
				if (i < 0 || i >= _instances.Count || i >= _instancesTime.Count) return null; // return if out of the structures boundaries

				if (_instances[i] != null && !_instances[i].activeSelf) // if not active instance
				{
					_result = _instances[i]; // set as a result
					_result.transform.position = _position; // update the position
					_result.SetActive(true); // activate
					_instancesTime[i] = Time.time; // update the time
					break;
				}

				if (_instancesTime[i] < _oldestInstanceTime) // if the instance time is older than the oldest one
				{
					_oldestInstanceTime = _instancesTime[i]; // update the oldest time
					_oldestInstanceIndex = i; // and its instance's index
				}
			}

			// If the result is not found and there is an oldest instance
			if (_result == null && _oldestInstanceIndex >= 0 && _oldestInstanceIndex < _instances.Count && _oldestInstanceIndex < _instancesTime.Count)
			{				
				_result = _instances[_oldestInstanceIndex]; // set the oldest as a result

				if (_result == null) return null; // return if does not exist

				_result.SetActive(false); // deactivate the result
				_result.transform.position = _position; // update the position
				_result.SetActive(true); // activate the result
				_instancesTime[_oldestInstanceIndex] = Time.time; // update the time
			}
		}
		
		return _result; // return the result
	}

	/// <summary>
	/// Pops the instance
	/// </summary>
	/// <returns>The popped instance</returns>
	/// <param name="_name">Name of a unique object</param>
	public GameObject PopInstance(string _name)
	{
		return PopInstance(_name, Vector3.zero); // return the popped instance
	}

	/// <summary>
	/// Pushes the instance
	/// </summary>
	/// <param name="_gameObject">An instance to push</param>
	public void PushInstance(GameObject _gameObject)
	{
		if (_gameObject) // if the game object exists
		{
			_gameObject.SetActive(false); // deactivate it
			_gameObject.transform.position = Vector3.one; // reset the position
			_gameObject.transform.rotation = Quaternion.identity; // reset the rotation
		}
	}

	/// <summary>
	/// Pushes the instance after a certain amount of time
	/// </summary>
	/// <param name="_gameObject">An instance to push</param>
	/// <param name="_time">Delay in pushing</param>
	public void PushInstanceAfter(GameObject _gameObject, float _time)
	{		
		StartCoroutine(PushInstanceAfterEnumerator(_gameObject, _time)); // start the coroutine
	}

	/// <summary>
	/// The enumerator for pushing the instance after a certain amount of time
	/// </summary>
	/// <param name="_gameObject">An instance to push</param>
	/// <param name="_time">Delay in pushing</param>
	protected IEnumerator PushInstanceAfterEnumerator(GameObject _gameObject, float _time)
	{
		yield return new WaitForSeconds(_time); // wait for a specific time
		PushInstance(_gameObject); // push the instance
	}
}

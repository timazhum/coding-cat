﻿/* 
 * Name: csPlayer.cs
 * Description: a component of a player
 * Author: Temirlan Zhumanov
 * Date: 23.04.2016
 * Email: lebrokholic@gmail.com
 * Copyright: All Rights Reserved.
 */

using UnityEngine;
using System.Collections;
using UnityEngine.SceneManagement;

public class csPlayer: csListener
{
	/// <summary>
	/// The character of a player
	/// </summary>
	public csCharacter _character = null;

	/// <summary>
	/// The money of a player
	/// </summary>
	protected int _money
	{
		get
		{
			return PlayerPrefs.GetInt("money", 1000);
		}
		set
		{
			PlayerPrefs.SetInt("money", value);
			PlayerPrefs.Save();
		}
	}

	/// <summary>
	/// The hurt speed in percentage per second
	/// </summary>
	public float _hurtSpeed = 0.01f;

	/// <summary>
	/// The discharge speed in percentage per second
	/// </summary>
	public float _dischargeSpeed = 0.02f;

	/// <summary>
	/// The GUI
	/// </summary>
	private csGUI _gui = null;

	/// <summary>
	/// The GUI reflection
	/// </summary>
	private csGUI _GUI { get { return _gui ?? (_gui = csClassFetcher<csGUI>.GetInstance()); } }

	/// <summary>
	/// The time of last update
	/// </summary>
	private float _timeLastUpdate = -Mathf.Infinity;

	/// <summary>
	/// The time delay in updates
	/// </summary>
	private float _timeDelay = 1f;

	/// <summary>
	/// Start this instance
	/// </summary>
	protected void Start()
	{
		csInput _input = csClassFetcher<csInput>.GetInstance();
		if (_input) _input.AddListener(this);

		_gui = csClassFetcher<csGUI>.GetInstance();
	}

	/// <summary>
	/// Updates the listener
	/// </summary>
	/// <param name="_arguments">Array of arguments</param>
	public override void UpdateListener(object[] _arguments)
	{
		if (_GUI == null || !csActivityMain.NAME.Equals(_GUI.GetCurrentActivityName())) return;

		csArgumentsReader _argumentsReader = new csArgumentsReader(_arguments);

		if (_argumentsReader != null)
		{
			bool _canMove = true;

			if (_argumentsReader.HasKey(csInput.ARGUMENT_TAPCOLLIDER))
			{
				object _value = _argumentsReader.GetValue(csInput.ARGUMENT_TAPCOLLIDER);
				if (_value is Collider)
				{
					Collider _collider = _value as Collider;
					if (_collider && _collider.tag == "Interactable")
					{
						_canMove = false;

						csInteractiveAccommodationItem _component = _collider.GetComponent<csInteractiveAccommodationItem>();
						if (_component) _component.Interact();
					}
				}
			}

			if (_argumentsReader.HasKey(csInput.ARGUMENT_TAPPOSITION))
			{
				object _value = _argumentsReader.GetValue(csInput.ARGUMENT_TAPPOSITION);
				if (_value is Vector3)
				{
					if (_canMove) Move((Vector3)_value);
				}
			}
		}
	}		

	/// <summary>
	/// Heal the character
	/// </summary>
	/// <param name="_amount">Amount of heal</param>
	public virtual void Heal(float _amount)
	{
		if (_character)
		{
			_character.Heal(_amount);

			csClassFetcher<csActivityMain>.GetInstance().UpdateUI(new csArgumentsBuilder().
				Add(csActivityMain.ARGUMENT_LIFEAMOUNT, _character.GetLifeAmount()).Build()
			);
		}
	}

	/// <summary>
	/// Hurt the character
	/// </summary>
	/// <param name="_amount">Amount of hurt</param>
	public virtual void Hurt(float _amount)
	{
		if (_character)
		{
			_character.Hurt(_amount);
		
			csClassFetcher<csActivityMain>.GetInstance().UpdateUI(new csArgumentsBuilder().
				Add(csActivityMain.ARGUMENT_LIFEAMOUNT, _character.GetLifeAmount()).Build()
			);

			if (_character.GetLifeAmount() <= 0f && _gui)
			{
				_gui.OpenActivity(csActivityDeath.NAME);
			}
		}
	}

	/// <summary>
	/// Charge the character
	/// </summary>
	/// <param name="_amount">Amount of charge</param>
	public virtual void Charge(float _amount)
	{
		if (_character)
		{
			_character.Charge(_amount);

			csClassFetcher<csActivityMain>.GetInstance().UpdateUI(new csArgumentsBuilder().
				Add(csActivityMain.ARGUMENT_ENERGYAMOUNT, _character.GetEnergyAmount()).Build()
			);
		}
	}

	/// <summary>
	/// Discharge the character
	/// </summary>
	/// <param name="_amount">Amount of discharge</param>
	public virtual void Discharge(float _amount)
	{
		if (_character)
		{
			_character.Discharge(_amount);

			csClassFetcher<csActivityMain>.GetInstance().UpdateUI(new csArgumentsBuilder().
				Add(csActivityMain.ARGUMENT_ENERGYAMOUNT, _character.GetEnergyAmount()).Build()
			);
		}
	}

	/// <summary>
	/// Move the character to a position
	/// </summary>
	/// <param name="_position">Position to move</param>
	public virtual void Move(Vector3 _position)
	{
		if (_character && _character.GetEnergyAmount() > 0f)
			_character.Move(_position);
	}

	/// <summary>
	/// Sets the character name
	/// </summary>
	/// <param name="_name">Name of a character</param>
	public virtual void SetCharacterName(string _name)
	{
		if (_character)
		{
			_character._name = _name;

			csActivityMain _activity = csClassFetcher<csActivityMain>.GetInstance();
			if (_activity) _activity.UpdateUI(new csArgumentsBuilder().
				Add(csActivityMain.ARGUMENT_CHARACTERNAME, _character._name).Build()
			);
		}
	}

	/// <summary>
	/// Adds the money amount
	/// </summary>
	/// <param name="_amount">Amount</param>
	public virtual void AddMoneyAmount(int _amount)
	{
		SetMoneyAmount(_money + _amount);
	}

	/// <summary>
	/// Reduces the money amount
	/// </summary>
	/// <param name="_amount">Amount</param>
	public virtual void ReduceMoneyAmount(int _amount)
	{
		SetMoneyAmount(_money - _amount);
	}

	/// <summary>
	/// Gets the money amount
	/// </summary>
	/// <returns>The money amount</returns>
	public virtual int GetMoneyAmount()
	{
		return this._money;
	}

	/// <summary>
	/// Sets the money amount
	/// </summary>
	/// <param name="_money">Money</param>
	protected virtual void SetMoneyAmount(int _money)
	{
		this._money = _money;

		csActivityMain _activity = csClassFetcher<csActivityMain>.GetInstance();
		if (_activity) _activity.UpdateUI(new csArgumentsBuilder().
			Add(csActivityMain.ARGUMENT_MONEYAMOUNT, _money).Build()
		);
	}

	/// <summary>
	/// Whether initialized or not
	/// </summary>
	private bool _initialized = false;

	/// <summary>
	/// Update this instance
	/// </summary>
	protected void Update()
	{
		const string _characterName = "Koding Kitty";

		if (!_initialized)
		{
			SetCharacterName(_characterName);
			SetMoneyAmount(_money);
			_initialized = true;
		}

		if (Time.time - _timeLastUpdate >= _timeDelay)
		{
			_timeLastUpdate = Time.time;
			Hurt(_hurtSpeed * _timeDelay);
			Discharge(_dischargeSpeed * _timeDelay);
		}			
	}
}	
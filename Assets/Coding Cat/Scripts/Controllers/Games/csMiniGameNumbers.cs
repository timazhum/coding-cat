﻿/* 
 * Name: csMiniGameNumber.cs
 * Description: a component of a numbers mini game
 * Author: Temirlan Zhumanov
 * Date: 24.04.2016
 * Email: lebrokholic@gmail.com
 * Copyright: All Rights Reserved.
 */

using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;
using System.Collections;
using System.Collections.Generic;

public class csMiniGameNumbers: csMiniGame
{
	/// <summary>
	/// Number base enum
	/// </summary>
	public enum NumberBase
	{
		Bin,
		Hex
	}

	/// <summary>
	/// The number base of a game
	/// </summary>
	public NumberBase _numberBase = NumberBase.Bin;

	/// <summary>
	/// The variants number
	/// </summary>
	public int _variantsNumber = 0;

	/// <summary>
	/// The guess number
	/// </summary>
	private int _guessNumber = 0;

	/// <summary>
	/// The guess number min
	/// </summary>
	private int _guessNumberMin = 1;

	/// <summary>
	/// The guess number max
	/// </summary>
	private int _guessNumberMax = 32;

	/// <summary>
	/// The iterations current number
	/// </summary>
	private int _iterationsCurrent = 0;

	/// <summary>
	/// The iterations max number
	/// </summary>
	private int _iterationsMax = 5;

	/// <summary>
	/// The success number
	/// </summary>
	private int _successNumber = 0;

	/// <summary>
	/// The success number for victory
	/// </summary>
	private int _successNumberForVictory = 3;

	/// <summary>
	/// The variants list
	/// </summary>
	private List<int> _variants = new List<int>();

	// Arguments keys
	public const string GUESSNUMBER = "guessnumber";
	public const string SUCCESSNUMBER = "successnumber";
	public const string ITERATIONSCURRENT = "iterationscurrent";
	public const string ITERATIONSMAX = "iterationsmax";
	public const string VARIANTS = "variants";

	/// <summary>
	/// Play the game
	/// </summary>
	public override void Play()
	{
		base.Play();

		Reset();
		Next();
	}

	/// <summary>
	/// Reset this instance
	/// </summary>
	protected override void Reset()
	{
		_iterationsCurrent = 0;
		_successNumber = 0;
		if (_variants != null)
		{
			_variants.Clear();
			for (int i = 0; i < _variantsNumber; i++)
				_variants.Add(0);
		}
	}

	/// <summary>
	/// Next question
	/// </summary>
	private void Next()
	{
		GenerateGuessNumber();
		GenerateVariants();

		_iterationsCurrent++;
		if (_iterationsCurrent > _iterationsMax) Stop();

		UpdateViews();
	}

	/// <summary>
	/// Generates the guess number
	/// </summary>
	protected void GenerateGuessNumber()
	{
		_guessNumber = Random.Range(_guessNumberMin, _guessNumberMax);
	}

	/// <summary>
	/// Generates the variants
	/// </summary>
	protected void GenerateVariants()
	{
		if (_variants != null)
		{
			int _variantIndex = Random.Range(0, _variants.Count);

			for (int i = 0; i < _variants.Count; i++)
			{
				if (i == _variantIndex)
					_variants[i] = _guessNumber;
				else
					_variants[i] = Random.Range(_guessNumberMin, _guessNumberMax);
			}
		}
	}

	/// <summary>
	/// Makes the guess
	/// </summary>
	/// <param name="_variantIndex">Variant index</param>
	public void MakeGuess(int _variantIndex)
	{
		if (_variants != null && _variantIndex >= 0 && _variantIndex < _variants.Count)
		{
			if (_variants[_variantIndex] == _guessNumber)
				Success();
			else
				Failure();

			Next();
		}
	}

	/// <summary>
	/// Success of this instance
	/// </summary>
	protected override void Success()
	{
		_successNumber++;
	}
		
	/// <summary>
	/// Failure of this instance
	/// </summary>
	protected override void Failure()
	{
		
	}

	/// <summary>
	/// Updates the view
	/// </summary>
	public override void UpdateView(csView _view)
	{
		string _guessNumberString = "";
		switch (_numberBase)
		{
			case NumberBase.Bin:
				_guessNumberString = System.Convert.ToString((int)_guessNumber, 2).PadLeft(8, '0') + "<size=24> 2</size>";
				break;
			case NumberBase.Hex:
				_guessNumberString = "0x" + _guessNumber.ToString("X");
				break;
		}

		if (_view)
			_view.UpdateView(new csArgumentsBuilder().
				Add(GUESSNUMBER, _guessNumberString).
				Add(SUCCESSNUMBER, _successNumber).
				Add(ITERATIONSCURRENT, _iterationsCurrent).
				Add(ITERATIONSMAX, _iterationsMax).
				Add(VARIANTS, _variants).Build());
	}

	/// <summary>
	/// Determines whether the game has been completed with a victory or not
	/// </summary>
	/// <returns>true</returns>
	/// <c>false</c>
	protected override bool IsVictory()
	{
		return _successNumber >= _successNumberForVictory;			
	}
}

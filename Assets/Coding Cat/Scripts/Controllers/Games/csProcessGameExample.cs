﻿/* 
 * Name: csProcessGameExample.cs
 * Description: a component of a process game example
 * Author: Temirlan Zhumanov
 * Date: 24.04.2016
 * Email: lebrokholic@gmail.com
 * Copyright: All Rights Reserved.
 */

using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;
using System.Collections;

public class csProcessGameExample: csProcessGame
{
	/// <summary>
	/// Play the game
	/// </summary>
	public override void Play()
	{

	}	

	/// <summary>
	/// Stop the game
	/// </summary>
	public override void Stop()
	{

	}

	/// <summary>
	/// Reset the game
	/// </summary>
	protected override void Reset()
	{

	}

	/// <summary>
	/// Success in the game
	/// </summary>
	protected override void Success()
	{

	}

	/// <summary>
	/// Failure in the game
	/// </summary>
	protected override void Failure()
	{

	}

	/// <summary>
	/// Updates the view
	/// </summary>
	public override void UpdateView(csView _view)
	{

	}

	/// <summary>
	/// Determines whether the game has been completed with a victory or not
	/// </summary>
	/// <returns>true</returns>
	/// <c>false</c>
	protected override bool IsVictory ()
	{
		return true;
	}
}

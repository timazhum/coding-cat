﻿/* 
 * Name: csMiniGameOperators.cs
 * Description: a component of an operators mini game
 * Author: Temirlan Zhumanov
 * Date: 24.04.2016
 * Email: lebrokholic@gmail.com
 * Copyright: All Rights Reserved.
 */

using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;
using System.Collections;
using System.Collections.Generic;

public class csMiniGameOperators: csMiniGame
{
	/// <summary>
	/// Operators enum
	/// </summary>
	public enum Operator
	{
		And,
		Or,
		Xor,
		Not
	}

	/// <summary>
	/// The operator of a game
	/// </summary>
	public Operator _operator = Operator.And;

	/// <summary>
	/// The numbers amount
	/// </summary>
	public int _numbersAmount = 0;

	/// <summary>
	/// The numbers
	/// </summary>
	private List<int> _numbers = new List<int>();

	/// <summary>
	/// The guess number
	/// </summary>
	private int _guessNumber = 0;

	/// <summary>
	/// The generated number min
	/// </summary>
	private int _numberMin = 0;

	/// <summary>
	/// The generated number max
	/// </summary>
	private int _numberMax = 15;

	/// <summary>
	/// The success number
	/// </summary>
	private int _successNumber = 0;

	/// <summary>
	/// The success number for victory
	/// </summary>
	private int _successNumberForVictory = 1;

	// Arguments keys
	public const string NUMBERS = "numbers";
	public const string GUESSNUMBER = "guessnumber";

	/// <summary>
	/// Play the game
	/// </summary>
	public override void Play()
	{
		base.Play();

		Reset();
		GenerateNumbers();
		UpdateViews();
	}

	/// <summary>
	/// Reset this instance
	/// </summary>
	protected override void Reset()
	{
		_guessNumber = _operator == Operator.And ? _numberMax : _numberMin;

		if (_numbers != null)
		{
			_numbers.Clear();
			for (int i = 0; i < _numbersAmount; i++)
				_numbers.Add(0);
		}
	}

	/// <summary>
	/// Generates the numbers
	/// </summary>
	protected void GenerateNumbers()
	{
		if (_numbers != null)
			for (int i = 0; i < _numbers.Count; i++)
				_numbers[i] = Random.Range(_numberMin, _numberMax);
	}

	/// <summary>
	/// Inverts the digit
	/// </summary>
	/// <param name="_position">Position of a digit</param>
	public void InvertDigit(int _position)
	{
		_guessNumber ^= (1 << _position);

		if (_numbers != null && _numbers.Count > 0)
		{
			int _result = _operator == Operator.Not ? ~_numbers[0] : _numbers[0];
			for (int i = 1; i < _numbers.Count; i++)
			{
				switch (_operator)
				{
					case Operator.And:
						_result &= _numbers[i];
						break;
					case Operator.Or:
						_result |= _numbers[i];
						break;
					case Operator.Xor:
						_result ^= _numbers[i];
						break;
					case Operator.Not:
						_result = ~_numbers[i];
						break;
				}
			}
			if (_operator == Operator.Not) _result = (16 + _result % 16) % 16;

			if (_guessNumber == _result) Success();
		}

		UpdateViews();
	}
		
	/// <summary>
	/// Success of this instance
	/// </summary>
	protected override void Success()
	{
		_successNumber++;
		Stop();
	}
		
	/// <summary>
	/// Failure of this instance
	/// </summary>
	protected override void Failure()
	{
		
	}

	/// <summary>
	/// Updates the view
	/// </summary>
	public override void UpdateView(csView _view)
	{		
		if (_view)
			_view.UpdateView(new csArgumentsBuilder().
				Add(NUMBERS, _numbers).
				Add(GUESSNUMBER, _guessNumber).Build());
	}

	/// <summary>
	/// Determines whether the game has been completed with a victory or not
	/// </summary>
	/// <returns>true</returns>
	/// <c>false</c>
	protected override bool IsVictory()
	{
		return _successNumber >= _successNumberForVictory;			
	}
}

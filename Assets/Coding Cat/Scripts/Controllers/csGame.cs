﻿/* 
 * Name: csGame.cs
 * Description: a component of a game
 * Author: Temirlan Zhumanov
 * Date: 24.04.2016
 * Email: lebrokholic@gmail.com
 * Copyright: All Rights Reserved.
 */

using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;
using System.Collections;

public abstract class csGame: csController
{
	/// <summary>
	/// The prize
	/// </summary>
	public csPrize _prize = null;

	/// <summary>
	/// Play the game
	/// </summary>
	public virtual void Play()
	{
		csGUI _gui = csClassFetcher<csGUI>.GetInstance();
		if (_gui) _gui.OpenActivity(csActivityGame.NAME);

		csActivityGame _activityGame = csClassFetcher<csActivityGame>.GetInstance();
		if (_activityGame)
			foreach (csView _view in _views)
				_activityGame.OpenView(_view);
	}	

	/// <summary>
	/// Stop the game
	/// </summary>
	public virtual void Stop()
	{
		csGUI _gui = csClassFetcher<csGUI>.GetInstance();
		if (_gui) _gui.CloseActivity();

		csActivityGame _activityGame = csClassFetcher<csActivityGame>.GetInstance();
		if (_activityGame) _activityGame.CloseViews();

		if (IsVictory())
		{
			Reward();
			if (_gui) _gui.OpenActivity(csActivityPrize.NAME);
		}
	}

	/// <summary>
	/// Reset the game
	/// </summary>
	protected abstract void Reset();

	/// <summary>
	/// Success in the game
	/// </summary>
	protected abstract void Success();

	/// <summary>
	/// Failure in the game
	/// </summary>
	protected abstract void Failure();

	/// <summary>
	/// Determines whether the game has been completed with a victory or not
	/// </summary>
	/// <returns><c>true</c> if the game has been completed with a victory; otherwise, <c>false</c></returns>
	protected abstract bool IsVictory();

	/// <summary>
	/// Reward the player
	/// </summary>
	protected virtual void Reward()
	{
		if (_prize) _prize.Reward();
	}
}

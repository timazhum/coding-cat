﻿/* 
 * Name: csInteractiveAccommodationItem.cs
 * Description: a component of an interactive accommodation item
 * Author: Temirlan Zhumanov
 * Date: 24.04.2016
 * Email: lebrokholic@gmail.com
 * Copyright: All Rights Reserved.
 */

using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;
using System.Collections;

public class csInteractiveAccommodationItem: csAccommodationItem
{
	public enum InteractionType
	{
		Switch
	};

	/// <summary>
	/// The type of the interaction
	/// </summary>
	public InteractionType _interactionType = InteractionType.Switch;

	/// <summary>
	/// The interaction object
	/// </summary>
	public Object _interactionObject = null;

	/// <summary>
	/// Interact this instance
	/// </summary>
	public void Interact()
	{
		if (_interactionType == InteractionType.Switch && _interactionObject is GameObject)
		{
			csApplication _application = csClassFetcher<csApplication>.GetInstance();
			if (_application) _application.Switch(_interactionObject as GameObject);
		}
	}
}

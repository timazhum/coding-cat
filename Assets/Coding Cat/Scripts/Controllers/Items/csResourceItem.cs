﻿/* 
 * Name: csResourceItem.cs
 * Description: a component of a resource item
 * Author: Temirlan Zhumanov
 * Date: 24.04.2016
 * Email: lebrokholic@gmail.com
 * Copyright: All Rights Reserved.
 */

using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;
using System.Collections;

public class csResourceItem: csItem
{
	/// <summary>
	/// Resource type enum
	/// </summary>
	public enum ResourceType
	{
		Energy,
		Health
	}

	/// <summary>
	/// The type of the resource
	/// </summary>
	public ResourceType _resourceType = ResourceType.Energy;

	/// <summary>
	/// Purchase the item
	/// </summary>
	public override void Purchase()
	{
		csPlayer _player = csClassFetcher<csPlayer>.GetInstance();
		if (_player && _player.GetMoneyAmount() >= _cost)
		{
			_player.ReduceMoneyAmount(_cost);
		
			switch (_resourceType)
			{
				case ResourceType.Energy:
					_player.Charge(1f);
					break;
				case ResourceType.Health:
					_player.Heal(1f);
					break;
			}
		}

		csGUI _gui = csClassFetcher<csGUI>.GetInstance();
		if (_gui) _gui.CloseActivity();
	}
}

﻿/* 
 * Name: csItem.cs
 * Description: a controller of an item
 * Author: Temirlan Zhumanov
 * Date: 24.04.2016
 * Email: lebrokholic@gmail.com
 * Copyright: All Rights Reserved.
 */

using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;
using System.Collections;

public abstract class csItem: csController
{
	/// <summary>
	/// The identifier of an item
	/// </summary>
	public string _id = "";

	/// <summary>
	/// The name of an item
	/// </summary>
	public string _name = "";

	/// <summary>
	/// The image of an item
	/// </summary>
	public Sprite _image = null;

	/// <summary>
	/// The cost of an item
	/// </summary>
	public int _cost = 0;

	/// <summary>
	/// Gets or sets a value indicating whether this <see cref="csItem"/> is purchased
	/// </summary>
	/// <value><c>true</c> if purchased; otherwise, <c>false</c></value>
	[HideInInspector]
	public bool _purchased
	{
		get
		{
			return PlayerPrefs.GetInt("purchased_" + _id, 0) > 0;
		}
		set
		{
			PlayerPrefs.SetInt("purchased_" + _id, value ? 1 : 0);
			PlayerPrefs.Save();
			UpdateViews();
		}
	}

	/// <summary>
	/// Gets or sets a value indicating whether this <see cref="csItem"/> is activated
	/// </summary>
	/// <value><c>true</c> if activated; otherwise, <c>false</c></value>
	[HideInInspector]
	public bool _activated
	{
		get
		{
			return PlayerPrefs.GetInt("activated_" + _id, 0) > 0;
		}
		set
		{
			PlayerPrefs.SetInt("activated_" + _id, value ? 1 : 0);
			PlayerPrefs.Save();
			UpdateViews();
		}
	}

	// Arguments keys
	public const string NAME = "name";
	public const string IMAGE = "image";
	public const string COST = "cost";
	public const string PURCHASED = "purchased";
	public const string ACTIVATED = "activated";

	/// <summary>
	/// Updates the view
	/// </summary>
	public override void UpdateView(csView _view)
	{
		if (_view)
			_view.UpdateView(new csArgumentsBuilder().
				Add(NAME, _name).
				Add(IMAGE, _image).
				Add(COST, _cost).
				Add(ACTIVATED, _activated).
				Add(PURCHASED, _purchased).Build());
	}

	/// <summary>
	/// Purchase the item
	/// </summary>
	public virtual void Purchase()
	{
		csPlayer _player = csClassFetcher<csPlayer>.GetInstance();
		if (_player && _player.GetMoneyAmount() >= _cost)
		{
			_player.ReduceMoneyAmount(_cost);
			_purchased = true;
		}
	}

	/// <summary>
	/// Start this instance
	/// </summary>
	protected void Start()
	{
		UpdateViews();
	}
}

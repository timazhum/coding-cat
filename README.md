# README #

This README would normally document whatever steps are necessary to get your application up and running.

### What is this repository for? ###

* Quick summary: a tamagotchi game about a coding cat
* Project name: Coding Cat
* Product name: Coding Cat

### How do I get set up? ###

* Clone this repository and open the project included in the Unity game engine
* Dependencies: Unity game engine (5.3.4f1)

### Contribution guidelines ###

* Mentor: [Dr. Mona Rizvi](mailto:maerizvi@gmail.com)

### Who do I talk to? ###

* [Temirlan Zhumanov](https://bitbucket.org/lebrokholic/)
* Facebook: http://facebook.com/timazhum
* Twitter: http://twitter.com/lebrokholic
* Skype: zhumanovtemirlan